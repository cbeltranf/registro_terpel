<?
function friendlyDate( $date, $lDay, $lMonth, $dispHour ) {
	if ( $lDay ) {
		$arrayDays = array( "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo" );
	} else {
		$arrayDays = array( "Lun.", "Mar.", "Mie.", "Jue.", "Vie.", "Sab.", "Dom." );
	}
	if ( $lMonth ) {
		$arrayMonth = array( "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" );
	} else {
		$arrayMonth = array( "ene.", "feb.", "mar.", "abr.", "may.", "jun.", "jul.", "ago.", "sep.", "oct.", "nov.", "dic." );
	}
	$hourFormatted = "";
	if ( $dispHour ) {
		$hourFormatted = " · " . date( 'H:i', strtotime( $date ) );
	}

  $day   = $arrayDays[ date( "N", strtotime( $date ) ) - 1 ];
  $month = $arrayMonth[ date( "n", strtotime( $date ) ) - 1 ];
  $year  =  date( "Y", strtotime( $date ) ) ;

	$friendlyFormat = $day . " " . date( 'd', strtotime( $date ) ) . " de " . $month . " de " . $year . $hourFormatted;

	return $friendlyFormat;
}


function sanitize_strings($var)
{
  $var = trim($var);
  $var = filter_var($var, FILTER_SANITIZE_STRING);
  return $var;
}

function sanitize_int($var)
{
  $var = trim($var);
  $var = filter_var($var, FILTER_VALIDATE_INT);
  return $var;
}

function sanitize_email($var)
{
  $var = trim($var);
  $var = filter_var($var, FILTER_SANITIZE_EMAIL);
  return $var;
}



function satinize_me($var, $type = 'int')
{
  if ($type === 'int') {
    $var  = filter_var(sanitize_int(str_replace("\n", "", str_replace("\r", "", trim($var)))), FILTER_SANITIZE_MAGIC_QUOTES);
  } else if($type === 'email'){
    $var  = filter_var(sanitize_email(str_replace("\n", "", str_replace("\r", "", trim($var)))), FILTER_SANITIZE_MAGIC_QUOTES);
  }else {
    $var  = filter_var(sanitize_strings(str_replace("\n", "", str_replace("\r", "", trim($var)))), FILTER_SANITIZE_MAGIC_QUOTES);
  }
  return $var;
}





// return the interpolated value between pBegin and pEnd
function interpolate( $pBegin, $pEnd, $pStep, $pMax ) {
	if ( $pBegin < $pEnd ) {
		return ( ( $pEnd - $pBegin ) * ( $pStep / $pMax ) ) + $pBegin;
	} else {
		return ( ( $pBegin - $pEnd ) * ( 1 - ( $pStep / $pMax ) ) ) + $pEnd;
	}
}

// generate gradient swathe now

////ee3524 - c41230
function colorScale( $theColorBegin, $theColorEnd, $theNumSteps ) {
	$ArrColor = array();
	$theColorBegin = hexdec( $theColorBegin );
	$theColorEnd = hexdec( $theColorEnd );
	$theNumSteps = $theNumSteps - 1;

	$theR0 = ( $theColorBegin & 0xff0000 ) >> 16;
	$theG0 = ( $theColorBegin & 0x00ff00 ) >> 8;
	$theB0 = ( $theColorBegin & 0x0000ff ) >> 0;

	$theR1 = ( $theColorEnd & 0xff0000 ) >> 16;
	$theG1 = ( $theColorEnd & 0x00ff00 ) >> 8;
	$theB1 = ( $theColorEnd & 0x0000ff ) >> 0;


	for ( $i = 0; $i <= $theNumSteps; $i++ ) {
		$theR = interpolate( $theR0, $theR1, $i, $theNumSteps );
		$theG = interpolate( $theG0, $theG1, $i, $theNumSteps );
		$theB = interpolate( $theB0, $theB1, $i, $theNumSteps );

		$theVal = ( ( ( $theR << 8 ) | $theG ) << 8 ) | $theB;
		$hexColor = sprintf( "%06X", $theVal );
		// echo $theVal  . "-" ;
		array_push( $ArrColor, "#" . $hexColor );
		//$ArrColor $theTDARTag . "-";

	}
	return $ArrColor;
}
//print_r(colorScale("ee3524","c41230",1))  

 function time_ago($timestamp)  {  
      $time_ago = strtotime($timestamp);  
      $current_time = time();  
      $time_difference = $current_time - $time_ago;  
      $seconds = $time_difference;  
      $minutes      = round($seconds / 60 );           // value 60 is seconds  
      $hours           = round($seconds / 3600);           //value 3600 is 60 minutes * 60 sec  
      $days          = round($seconds / 86400);          //86400 = 24 * 60 * 60;  
      $weeks          = round($seconds / 604800);          // 7*24*60*60;  
      $months          = round($seconds / 2629440);     //((365+365+365+365+366)/5/12)*24*60*60  
      $years          = round($seconds / 31553280);     //(365+365+365+365+366)/5 * 24 * 60 * 60  
      if($seconds <= 60)  
      {  
     return "Justo ahora";  
   }  
      else if($minutes <=60)  
      {  
     if($minutes==1)  
           {  
       return "Hace un minuto";  
     }  
     else  
           {  
       return "Hace $minutes minutos";  
     }  
   }  
      else if($hours <=24)  
      {  
     if($hours==1)  
           {  
       return "Hace una hora";  
     }  
           else  
           {  
       return "Hace $hours horas";  
     }  
   }  
      else if($days <= 7)  
      {  
     if($days==1)  
           {  
       return "Ayer";  
     }  
           else  
           {  
       return "Hace $days días";  
     }  
   }  
      else if($weeks <= 4.3) //4.3 == 52/12  
      {  
     if($weeks==1)  
           {  
       return "Hace una semana";  
     }  
           else  
           {  
       return "Hace $weeks semanas";  
     }  
   }  
       else if($months <=12)  
      {  
     if($months==1)  
           {  
       return "Hace un mes";  
     }  
           else  
           {  
       return "Hace $months meses";  
     }  
   }  
      else  
      {  
     if($years==1)  
           {  
       return "Hace un año";  
     }  
           else  
           {  
       return "Hace $years años";  
     }  
   }  
 }


function diffBetweenTimes($inDate1, $inDate2){
	$date1 = new DateTime( $inDate1 );
	$date2 = new DateTime( $inDate2 );

	$diff = $date2->diff($date1);

	$dayLabel = "";
	$hasDays = $diff->format('%a');
	if ($hasDays > 1){
	  $dayLabel = " días ";
	}
	elseif($hasDays=="1"){
		$dayLabel = "día ";
	}
	$timeElapsed = ($dayLabel!="") ? $hasDays . " " . $dayLabel : "";
	$timeElapsed .= $diff->format('%H:%I:%S');
	
	return $timeElapsed;
}



function getUserIpAddr(){
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
