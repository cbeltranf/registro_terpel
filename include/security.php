<?php
ini_set('session.cookie_lifetime',3600*8); 
ini_set('session.gc_maxlifetime', 3600*8);
error_reporting(0);
date_default_timezone_set("America/Bogota");
session_start();
if($_SESSION['auth_register']!='true'){	
	require('mysql_class.php');
	require('utils.php');
	$ip     = getUserIpAddr();
	$sql = "INSERT INTO `LOG` (`action`,`extra_info`, `ip`, `USER_id`,`agent`,`script`) VALUES ('register_login','403: Forbidden', '$ip', '$_SESSION[nit]', '$_SERVER[HTTP_USER_AGENT]','$_SERVER[SCRIPT_FILENAME]'); ";
	$micon->query($sql);
	
	session_destroy();
	header("Location: index.php?e=3");
}