<?php
session_start();
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
date_default_timezone_set("America/Bogota");
require_once('setup.php');
class DB_mysql
{
    /* variables de conexi�n */
    var $dataBase;
    var $server;
    var $user;
    var $password;
    /* identificador de conexi�n y consulta */
    var $link = 0;
    var $queryID;
    /* n�mero de error y texto error */
    var $Errno = 0;
    var $Error = "";
    
    
    /// variables de log 
    var $IpLog;
    var $UserLog;
    var $PageLog;
    
    /* M�todo Constructor: Cada vez que creemos una variable de esta clase, se ejecutar� esta funci�n */
    function DB_mysql($bd = "", $host = "localhost", $user = "nobody", $pass = "")
    {
        $this->dataBase = $bd;
        $this->server   = $host;
        $this->user     = $user;
        $this->password = $pass;
    }
    /*Conexi�n a la base de datos*/
    function connect($bd, $host, $user, $pass)
    {	
    	//echo $bd, $host, $user, $pass;
        if ($bd != "")
            $this->dataBase = $bd;
        if ($host != "")
            $this->server = $host;
        if ($user != "")
            $this->user = $user;
        if ($pass != "")
            $this->password = $pass;
        // Conectamos al server
        $this->link = mysqli_connect($this->server, $this->user, $this->password);
       // echo  $this->link;
        if (!$this->link) {
            $this->Error = "Ha fallado la conexi�n.";
            //echo $this->Error;
            return 0;
        }
        //seleccionamos la base de datos
        if (!@mysqli_select_db($this->link, $this->dataBase)) {
            $this->Error = "Imposible abrir " . $this->dataBase;
           // echo $this->Error;
            return 0;
        }
        /* Si hemos tenido �xito conectando devuelve el identificador de la conexi�n, sino devuelve 0 */
        
        return $this->link;
    }
    function close()
    {
        //$this->$queryID->close();
        /* Si hemos tenido �xito conectando devuelve el identificador de la conexi�n, sino devuelve 0 */
        return mysqli_close($this->link);
    }
    
    /* Ejecuta un consulta */
    function query($sql = "")
    {
        if ($sql == "") {
            $this->Error = "No ha especificado una consulta SQL";
            return 0;
        }
        //ejecutamos la consulta
        
        //$sqlG = preg_replace('/\s+/', ' ', trim($sql));
        //$Accion = explode(' ',$sqlG);
        //print_r($Accion);
        
        $this->queryID = @mysqli_query($this->link, $sql);
        //echo $sql;
        if (!$this->queryID) {
            $this->Errno = mysqli_errno($this->link);
            $this->Error = mysqli_error($this->link);
        }
        /* Si hemos tenido �xito en la consulta devuelve el identificador de la conexi�n, sino devuelve 0 */
        return $this->queryID;
      //  return $this->queryID;
    }
    
    //devuelve el error
    function getError()
    {
        return $this->Error;
    }
    /* Devuelve el n�mero de campos de una consulta */
    function numFields()
    {
        return mysqli_num_fields($this->queryID);
    }
    /* Devuelve el n�mero de registros de una consulta */
    function numRows()
    {   

        return mysqli_num_rows($this->queryID);
    }
    /* Devuelve el nombre de un campo de una consulta */
    function fieldName($numcampo)
    {
        return mysqli_field_name($this->queryID, $numcampo);
    }
    /* Muestra los datos de una consulta */
    
    
    function fetchArray()
    {
        return mysqli_fetch_array($this->queryID);
    }
    function lastInsertID()
    {
        return mysqli_insert_id($this->link); 
    }
    
    
} //fin de la Clse DB_mysql

