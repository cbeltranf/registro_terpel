<?php
session_start();
require('../include/httpscheck.php');
require('../include/mysql_class.php');
require('../include/utils.php');
$ip     = getUserIpAddr();
$email = satinize_me($_POST["email"], $type = 'email'); 
$password = satinize_me($_POST["password"], $type = 'string'); 

if ($email != ""){ 
	
	$sql = " SELECT * FROM `ZONELEADER` WHERE `email` ='" . $email . "'";
	$micon->query($sql);
	$validate = $micon->fetchArray();
	
	if (password_verify($password, $validate['password'] )) {
		//echo '¡La contraseña es válida!';
		$_SESSION["id"]        = $validate['id'] ;

		$timestamp_login = date('Y-m-d H:i:s');
			
		$sql = "UPDATE `ZONELEADER` SET `last_login_date` = '$timestamp_login' WHERE `id` = '". $validate['id']."'";
		$micon->query($sql);

		$sql = "INSERT INTO `LOG` (`action`,`extra_info`, `ip`, `USER_id`,`agent`,`script`) VALUES ('register_login','200: OK', '$ip', '".$validate['id']."' , '$_SERVER[HTTP_USER_AGENT]','$_SERVER[SCRIPT_FILENAME]'); ";	
		$micon->query($sql);

		$_SESSION['auth_register'] = 'true';		
		$_SESSION["name"] = $validate["name"];
		$_SESSION['email'] = $validate['email'];

		header("Location: ../dashboard.php");

	}else{
			
		    $sql = "INSERT INTO `LOG` (`action`,`extra_info`, `ip`, `USER_id`,`agent`,`script`) VALUES ('register_login','404: Not Found', '$ip', '".$validate['id']."', '$_SERVER[HTTP_USER_AGENT]','$_SERVER[SCRIPT_FILENAME]'); ";
		    $micon->query($sql);
			$validate = '';
			
			$_SESSION['email'] = $validate['email'];	
			header("Location: ../index.php?e=1");	
	}
}
else{
	

	$sql = "INSERT INTO `LOG` (`action`,`extra_info`, `ip`, `USER_id`,`agent`,`script`) VALUES ('register_login','406: Not Acceptable', '$ip', '$email', '$_SERVER[HTTP_USER_AGENT]','$_SERVER[SCRIPT_FILENAME]'); ";	
    $micon->query($sql);

	header("Location: ../index.php?e=2");	
}
exit();	  