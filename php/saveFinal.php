<?
session_start();
require_once("../include/mysql_class.php");

//INICIALIZADORES
//$fileNameText = "Abrir enlace</a>";//NOMBRE ARCHIVO
function getStamp(){
  $now = (string)microtime();
  $now = explode(' ', $now);
  $mm = explode('.', $now[0]);
  $mm = $mm[1];
  $now = $now[1];
  $segundos = $now % 60;
  $segundos = $segundos < 10 ? "$segundos" : $segundos;
  return strval(date("YmdHi",mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"))) . "$segundos$mm");
}


$result = false;
$response = array( "result" => $result );//RESPUESTA

if ($_SESSION[id]!=''){
	$validSession = true;


	$hDoc     = filter_var($_POST['hDoc'], FILTER_SANITIZE_STRING);
	$fileName = "p0-" . $hDoc . '-' . getStamp(); 

	$sql = "SELECT
	    *
	FROM
	    `registration`
	    WHERE document='$hDoc'";

	$micon->query( $sql );
	$regInfo = $micon->fetchArray();
	
	//echo $sql;
	//print_r($regInfo);

	//INFO GENERAL
	$txtFullName     = filter_var( trim($_POST['txtFullName']), FILTER_SANITIZE_STRING);	
	$rdoHasPassPort0 = filter_var($_POST['rdoHasPassPort0'], FILTER_SANITIZE_STRING);
	$txtPassport0    = filter_var($_POST['txtPassport0'], FILTER_SANITIZE_STRING);
	$txtBDay         = filter_var($_POST['txtBDay'], FILTER_SANITIZE_STRING);
	$txtEMail        = filter_var($_POST['txtEMail'], FILTER_SANITIZE_STRING);
	$txtCity         = filter_var($_POST['txtCity'], FILTER_SANITIZE_STRING);
	$txtBussiness    = filter_var($_POST['txtBussiness'], FILTER_SANITIZE_STRING);
	$txtRegionalName = filter_var($_POST['txtRegionalName'], FILTER_SANITIZE_STRING);
	$txtSapCode      = filter_var($_POST['txtSapCode'], FILTER_SANITIZE_STRING);
	$txtUniqueCode   = filter_var($_POST['txtUniqueCode'], FILTER_SANITIZE_STRING);
	$txtEDS          = filter_var($_POST['txtEDS'], FILTER_SANITIZE_STRING);
	$txtAdress       = filter_var($_POST['txtAdress'], FILTER_SANITIZE_STRING);
	$txtMobilePhone  = filter_var($_POST['txtMobilePhone'], FILTER_SANITIZE_STRING);
	$txtLandPhone    = filter_var($_POST['txtLandPhone'], FILTER_SANITIZE_STRING);



	//ACOMODACIÓN
	$txtNRoomSimple  = filter_var( ($_POST['txtNRoomSimple']!='') ? $_POST['txtNRoomSimple'] : 0, FILTER_SANITIZE_NUMBER_INT);
	$txtNRoomDouble  = filter_var( ($_POST['txtNRoomDouble']!='') ? $_POST['txtNRoomDouble'] : 0 , FILTER_SANITIZE_NUMBER_INT);
	$txtNRoomTriple  = filter_var( ($_POST['txtNRoomTriple']!='') ? $_POST['txtNRoomTriple'] : 0 , FILTER_SANITIZE_NUMBER_INT);

	//EXTENSIONES
	$slExtension     = ($_POST['slExtension']!="") ? filter_var($_POST['slExtension'], FILTER_SANITIZE_STRING) : "0";
	$rdoExtension    = filter_var($_POST['rdoExtension'], FILTER_SANITIZE_STRING);

	//ACOMPAÑANTES
	$rdoCompanion    = filter_var($_POST['rdoCompanion'], FILTER_SANITIZE_STRING);

	//PAGOS
	$rdoPayment = filter_var($_POST['rdoPayment'], FILTER_SANITIZE_STRING);

	$txtQ22     = filter_var($_POST['txtQ22'], FILTER_SANITIZE_STRING);
	$txtQ23     = filter_var($_POST['txtQ23'], FILTER_SANITIZE_STRING);

	$txt21      = filter_var($_POST['txt21'], FILTER_SANITIZE_STRING);
	$txtMoney22 = filter_var($_POST['txtMoney22'], FILTER_SANITIZE_STRING);
	$txtMoney23 = filter_var($_POST['txtMoney23'], FILTER_SANITIZE_STRING);


	$txtStartQuotingDate = filter_var($_POST['txtStartQuotingDate'], FILTER_SANITIZE_STRING);
	$txtFinishQuotingDate = filter_var($_POST['txtFinishQuotingDate'], FILTER_SANITIZE_STRING);




	if ($txtQ22!=""){
		$quotes = $txtQ22;
	}
	if ($txtQ23!=""){
		$quotes = $txtQ23;
	}

	if ($txt21!=""){
		$money = $txt21;
	}
	if ($txtMoney22!=""){
		$money = $txtMoney22;
	}
	if ($txtMoney23!=""){
		$money = $txtMoney23;
	}




	//UPLOAD FILE
	$dirname = "../../travel_docs";

	$file0 = true;
	$file1 = true;
	$file2 = true;

	$file_passPort0 = ($regInfo[passport_file]!='') ? $regInfo[passport_file] : '' ;

	if ($_FILES['file-passPort0'][name]!=""){
		//$txtWTLink
		$fileExtension = pathinfo($_FILES['file-passPort0'][name], PATHINFO_EXTENSION);
		//$dirname .= "/".$formName."/" . date(Y) . "/" . date(m);

		
		mkdir( $dirname, 0777, true );

		//$fileName = "p0-" . $fileName;
		if ( move_uploaded_file( $_FILES[ 'file-passPort0' ][ "tmp_name" ], $dirname . "/" . $fileName . "." . $fileExtension ) ) {
			$file0 = true;
			$file_passPort0 =  $fileName . "." . $fileExtension;
			
		}
		else{
			$file0 = false;
		}
	}

	if ($rdoHasPassPort0 == "no"){
		$file_passPort0 = '';
		$txtPassport0   = '';

	}

	

	$sql = "UPDATE `registration` SET `regional` = '$txtRegionalName' , `name` = '$txtFullName' , `last_name` = '' , `birthday` = '$txtBDay' , `email` = '$txtEMail' , `city` = '$txtCity' , `bussiness` = '$txtBussiness' , `eds_name` = '$txtEDS' , `sap_code` = '$txtSapCode' , `unique_code` = '$txtUniqueCode' , `adress` = '$txtAdress' , `phone` = '$txtLandPhone' , `cellphone` = '$txtMobilePhone' , `simple_room` = '$txtNRoomSimple' , `double_room` = '$txtNRoomDouble' , `triple_room` = '$txtNRoomTriple' , `extension` = '$slExtension' , `extension_detail` = '$rdoExtension' , `has_passport` = '$rdoHasPassPort0' , `passport_number` = '$txtPassport0' , `passport_file` = '$file_passPort0' , `payment` = '$rdoPayment' , `quotes` = '$quotes' , `money` = '$money' , `start_quotes_date` = '$txtStartQuotingDate' , `end_quotes_date` = '$txtFinishQuotingDate' , `companions` = '$rdoCompanion'  WHERE `document` = '$hDoc'; ";

	//echo $sql ;


	if($micon->query($sql)){
		$result = true;	
		$sqlDel1 = "";
		if ($rdoCompanion == 0){
			//$hDoc
			$sqlDel = "delete from companions where registration_id = '$hDoc'; ";
			$micon->query($sqlDel);
		}
		if ($rdoCompanion == 1 || $rdoCompanion == 2){
			$sqlC1 = "SELECT id, passport_image
					FROM `companions`
					WHERE registration_id='$hDoc'
					ORDER BY id asc
					LIMIT 0,1";
			$micon->query( $sqlC1 );

				$regPassport    = $micon->fetchArray();
				$id             = $regPassport[id];
				$passport_image = $regPassport[passport_image];

			$sqlDel1 = "delete from `companions` where id = '$id'; ";
			//$sqlDel = "delete from companions where registration_id = '$hDoc'; ";
			

			$file_passPort1 = ( $passport_image !='') ? $passport_image : '' ;

			if ($_FILES['file-passPort1'][name]!=""){
				
				$fileExtension = pathinfo($_FILES['file-passPort1'][name], PATHINFO_EXTENSION);
				

				
				mkdir( $dirname, 0777, true );
				$fileName = "p1-" . $hDoc . '-' . getStamp(); 

				if ( move_uploaded_file( $_FILES[ 'file-passPort1' ][ "tmp_name" ], $dirname . "/" .  $fileName . "." . $fileExtension ) ) {
					$file1 = true;
					$file_passPort1 = $fileName . "." . $fileExtension;
					
				}
				else{
					$file1 = false;
				}
			}


			$txtCompanion1   = filter_var($_POST['txtCompanion1'], FILTER_SANITIZE_STRING);
			$rdoHasPassPort1 = filter_var($_POST['rdoHasPassPort1'], FILTER_SANITIZE_STRING);
			$txtPassport1    = filter_var($_POST['txtPassport1'], FILTER_SANITIZE_STRING);
			$rdoAgeRange1    = filter_var($_POST['rdoAgeRange1'], FILTER_SANITIZE_STRING);
			$txtAge1         = filter_var($_POST['txtAge1'], FILTER_SANITIZE_STRING);
			
			//	echo $rdoHasPassPort1;
			if ($rdoHasPassPort1 == "N"){
				$file_passPort1 = '';
				$txtPassport1   = '';				
			}

			if ($rdoCompanion == 1){
				$sqlC2 = "SELECT id
						FROM `companions`
						WHERE registration_id='$hDoc'
						ORDER BY id asc
						LIMIT 1,1";
				$micon->query( $sqlC2 );

				$regPassport    = $micon->fetchArray();
				$id             = $regPassport[id];



				$micon->query($sqlDel1); //del companion 1

				$sqlDel2 = "delete from `companions` where id = '$id'; ";
				$micon->query($sqlDel2); //del companion 2
				
			}




			$sqlInsert1       = "INSERT INTO `companions` (`name`, `type`, `age`, `passport_number`, `have_passport`, `passport_image`, `registration_id`) VALUES ('$txtCompanion1', '$rdoAgeRange1', '$txtAge1', '$txtPassport1', '$rdoHasPassPort1', '$file_passPort1', '$hDoc');  ";

			if ($rdoCompanion == 1){
				$micon->query($sqlInsert1);
			}

			//echo $sqlInsert . "\n";



		}
		if ($rdoCompanion == 2){

			

			$sqlC2 = "SELECT id, passport_image
					FROM `companions`
					WHERE registration_id='$hDoc'
					ORDER BY id asc
					LIMIT 1,1";
			$micon->query( $sqlC2 );

			$regPassport    = $micon->fetchArray();
			$id             = $regPassport[id];
			$passport_image = $regPassport[passport_image];

			$sqlDel = "delete from `companions` where id = '$id'; ";
			$micon->query($sqlDel);

			$file_passPort2 = ( $passport_image !='') ? $passport_image : '' ;

			if ($_FILES['file-passPort2'][name]!=""){
				
				$fileExtension = pathinfo($_FILES['file-passPort2'][name], PATHINFO_EXTENSION);
				

				
				mkdir( $dirname, 0777, true );
				$fileName = "p2-" . $hDoc . '-' . getStamp(); 

				if ( move_uploaded_file( $_FILES[ 'file-passPort2' ][ "tmp_name" ], $dirname . "/" .  $fileName . "." . $fileExtension ) ) {
					$file2 = true;
					$file_passPort2 = $fileName . "." . $fileExtension;
					
				}
				else{
					$file2 = false;
				}
			}


			$txtCompanion2   = filter_var($_POST['txtCompanion2'], FILTER_SANITIZE_STRING);
			$rdoHasPassPort2 = filter_var($_POST['rdoHasPassPort2'], FILTER_SANITIZE_STRING);
			$txtPassport2    = filter_var($_POST['txtPassport2'], FILTER_SANITIZE_STRING);
			$rdoAgeRange2    = filter_var($_POST['rdoAgeRange2'], FILTER_SANITIZE_STRING);
			$txtAge2         = filter_var($_POST['txtAge2'], FILTER_SANITIZE_STRING);
			
			if ($rdoHasPassPort2 == "N"){
				$file_passPort2 = '';
				$txtPassport2   = '';				
			}

			$micon->query($sqlDel1); //del companion 1
			$micon->query($sqlInsert1);
			$sqlInsert2       = "INSERT INTO `companions` (`name`, `type`, `age`, `passport_number`, `have_passport`, `passport_image`, `registration_id`) VALUES ('$txtCompanion2', '$rdoAgeRange2', '$txtAge2', '$txtPassport2', '$rdoHasPassPort2', '$file_passPort2', '$hDoc');  ";
			$micon->query($sqlInsert2);

			//echo $sqlInsert;

		}

		
		if($file0 && $file1 && $file2){
			$sr = 'c';
		}
		else{
			$sr = 'd';
		}
		$sqlUStatus = "UPDATE `registration` SET `status_reg` = '$sr'   WHERE `document` = '$hDoc'; ";
		$micon->query($sqlUStatus);
		//echo $sqlUStatus;

	}

}
else{
	$validSession = false;	
	$result       = false;	
}






$response = array( 
	"result"       => $result,
	"file0"        => $file0,  
	"file1"        => $file1,  
	"file2"        => $file2,  
	"validSession" => $validSession
); 

echo json_encode( $response ); 