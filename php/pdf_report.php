<?php 
session_start();
//require('../include/security.php'); 
require('../include/utils.php');
require('../include/mysql_class.php');
require('../vendor/tecnickcom/tcpdf/tcpdf.php');
require('custom_pdf_class.php');

$id = satinize_me($_GET['id'], 'int');  

$sql = "SELECT r.* FROM `registration` AS r
        	INNER JOIN `GASSTATION` AS g ON r.`gasstation_id` = g.`id`
        WHERE g.`ZONELEADER_id` =  '".$_SESSION["id"]."' AND r.document='$id'";

$micon->query( $sql );
$regInfo = $micon->fetchArray();
$count = $micon->numRows();

if($count > 0){

$styles_table = '<style>
table{
	border: 0.5px solid #dee2e6;
	border-collapse: collapse;
	width:100%;
	
}

td{
border: 0.5px solid #dee2e6;
padding: 5px 9px 5px 9px;
text-align:left;
font-size: 6.5px;
color: #939393;
font-weight: 500;
font-family: helvetica;
vertical-align:middle;
line-height:10px;
} 
span{
color: #383838;
font-weight: bold;
}
div{	
font-size: 7.5px;
text-align:center;
padding: 8px;
}
</style>';


$styles_table2 = '<style>
table{
	border: 0.5px solid #dee2e6;
	border-collapse: collapse;
	width:100%;
	
}

td{
padding: 20px 12px 20px 12px;
text-align:left;
font-size: 6.5px;
color: #AEAEAE;
font-weight: 500;
font-family: helvetica;
vertical-align:middle;
line-height:10px;
} 
span{
color: #383838;
font-weight: bold;
}
div{	
font-size: 7.5px;
text-align:center;
padding: 8px;
}
</style>';

		
	$sql = "SELECT	* 	FROM
	`companions`
	WHERE registration_id='$id'";

	$micon->query( $sql );
	$dataCompa = array();
	while($regCompa = $micon->fetchArray()){
	$objCompa                  = new stdClass();
	$objCompa->name            = $regCompa[ name ].' '.$regCompa[ "last_name" ];
	$objCompa->type            = $regCompa[ type ];
	$objCompa->age             = $regCompa[ age ];
	$objCompa->passport_number = $regCompa[ passport_number ];
	$objCompa->have_passport   = $regCompa[ have_passport ];
	$objCompa->passport_image  = $regCompa[ passport_image ];

	array_push( $dataCompa, $objCompa );
	}


	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetTitle(' ');
	$pdf->SetTitleFontSize(15);
	$pdf->SetAuthor('Carlos Andres Beltrán Franco - carlosbeltran@ingenieros.com - INMOV SAS');
	$pdf->SetSubject('Registrarion');
	$pdf->SetKeywords('Report, terpel, visit, agent');
	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, 30.3, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	
	/*
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);*/
	
	$pdf->SetFont('helvetica', 'R', 9);

	$ml = 15;


	
// Page 1 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	$pdf->AddPage();
	$pdf->Ln(1);
	$pdf->SetFont('helvetica', 'R', 9);
	$pdf->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(229, 48, 39)));
	$pdf->setCellPaddings(17, 0,0, 0);
	$pdf->SetFillColor(229, 48, 39);
	$pdf->SetTextColor(255,255,255);
	$pdf->SetX(0);
	$text="1. Información General";
	$pdf->Cell(90, 5, $text, 1, 1, 'L', 1, 0);
	$pdf->Ln();

	/*
	$pdf->Rect(0, 30, 90, 7,'F',array(),array(229, 48, 39));
	$pdf->SetXY($ml,30);
	$pdf->SetTextColor(255,255,255);
	$pdf->Write(7,'1. Información General');
	$pdf->SetTextColor(0,0,0);
	
	$pdf->SetXY($ml,40);	
	*/
	
	$ron= '<img src="../images/icons/radioon.png"  width="6" height="6" />';
	$roff= '<img src="../images/icons/radiooff.png" width="6" height="6"  />';

	$con= '<img src="../images/icons/checkon.png"  width="6" height="6" />';
	$coff= '<img src="../images/icons/checkoff.png" width="6" height="6"  />';

			if($regInfo[has_passport]=='yes'){
				$img1= $ron;
				$img2= $roff;
				/*$pdf->SetXY(40,50.5);	
				$pdf->RadioButton('passport', 5, array(), array(), 'Si', true);*/
				//$pdf->CheckBox('Si', 5, true, array(), array(), 'OK');
			}else{
				
				$img1= $roff;
				$img2= $ron;
				/*$pdf->SetXY(51,50.5);	
				$pdf->RadioButton('passport', 5, array(), array(), 'No', true);*/
			
			}
		
	$acompaniantes = '';
	if($regInfo["companions"] >= 0){
		
		if($dataCompa[0]->have_passport=='S'){
			$radio_acom1= $ron;
			$radio_acom2= $roff;
		}else{
			$radio_acom1= $roff;
			$radio_acom2= $ron;		
		}


		if($dataCompa[0]->type=='Adulto'){
			$rta1= $ron;
			$rta2= $roff;
			$edad = '';
		}else{
			$rta1= $roff;
			$rta2= $ron;		
			$edad = $dataCompa[0]->age ;
		}



		$acompaniantes = '
		<tr>
								<td colspan="2">Nombre Completo acompañante No 1  <br> <span>'.$dataCompa[0]->name.'</span>
								</td>
								<td>¿Tiene pasaporte?  &nbsp;  Si   &nbsp; '.$radio_acom1.' &nbsp;    -  No  &nbsp;'.$radio_acom2.'   </td>
							</tr>
							<tr>
								<td>No. Pasaporte  <br> <span>'.$dataCompa[0]->passport_number.'</span> </td>
								<td>Adulto   &nbsp; '.$rta1.' &nbsp;    -   Menor de edad  &nbsp;'.$rta2.'   </td>
								<td>Si es menor de edad especifique la edad <br> <span>'.$edad.'</span> </td>
							</tr>
							';

	}
	
	if($regInfo["companions"] == 2){
		
		if($dataCompa[1]->have_passport=='S'){
			$radio_acom1= $ron;
			$radio_acom2= $roff;
		}else{
			$radio_acom1= $roff;
			$radio_acom2= $ron;		
		}

		if($dataCompa[1]->type=='Adulto'){
			$rta1= $ron;
			$rta2= $roff;
			$edad = '';
		}else{
			$rta1= $roff;
			$rta2= $ron;		
			$edad = $dataCompa[1]->age ;
		}

		$acompaniantes .= '
		<tr>
			<td colspan="2">Nombre Completo acompañante No 2  <br>
			<span>'.$dataCompa[1]->name.'</span>
			</td>
			<td>¿Tiene pasaporte?  &nbsp;  Si   &nbsp; '.$radio_acom1.' &nbsp;    -  No  &nbsp;'.$radio_acom2.'   </td>
		</tr>
		<tr>
			<td> No. Pasaporte  <br>
			<span>'.$dataCompa[1]->passport_number.'</span> </td>
			<td>Adulto   &nbsp; '.$rta1.' &nbsp;    -   Menor de edad  &nbsp;'.$rta2.'   </td>
			<td>Si es menor de edad especifique la edad <br>
			<span>'.$edad.'</span> </td>
		</tr>
		';

	}
	

	$html = $styles_table.'
<table width="100%" cellpadding="5px">
<tr>
	<td colspan="2">
			Nombres y Apellidos Completos <br>
			<span>'.trim($regInfo["name"]) . " " . trim($regInfo["last_name"]).'</span>
	</td>
	<td valign="middle">No. de Cédula     <br>
	<span>'.trim($regInfo["document"]).'</span>        </td>
</tr> 
<tr>
	<td>¿Tiene pasaporte?  &nbsp;  Si   &nbsp; '.$img1.' &nbsp;    -  No  &nbsp;'.$img2.'   </td>
	<td colspan="2">No. Pasaporte    <br>
	<span>'.trim($regInfo["passport_number"]).'</span>     </td>
</tr>
<tr>
	<td>Fecha de nacimiento   <br>
	<span>'.trim($regInfo["birthday"]).'</span>    </td>
	<td>Correo electrónico   <br>
	<span>'.trim($regInfo["email"]).'</span>    </td>
	<td>Ciudad   <br>
	<span>'.trim($regInfo["city"]).'</span>    </td>
</tr>
<tr>
	<td>Razón social   <br>
	<span>'.trim($regInfo["bussiness"]).'</span>   </td> 
	<td>Nit     <br>
	<span>'.trim($regInfo["nit"]).'</span>  </td>
	<td>Nombre Completo EDS    <br>
	<span>'.trim($regInfo["eds_name"]).'</span>   </td>
</tr>
<tr>
	<td>Regional      <br>
	<span>'.trim($regInfo["regional"]).'</span>  </td>
	<td>Código SAP     <br>
	<span>'.trim($regInfo["sap_code"]).'</span> </td>
	<td>Código Único    <br>
	<span>'.trim($regInfo["unique_code"]).'</span> </td>
</tr>
<tr>
	<td>Dirección     <br>
	<span>'.trim($regInfo["adress"]).'</span> </td>
	<td>Celular    <br>
	<span>'.trim($regInfo["cellphone"]).'</span> </td>
	<td>Teléfono Fijo     <br>
	<span>'.trim($regInfo["phone"]).'</span> </td>
</tr>
<tr>
	<td colspan="3">
	<div>
	Identifica las personas que viajarán a la Convención TERPEL 2019 y la acomodación deseada. Recuerda que cada una de las personas debe firmar la
	autorización de manejo de datos que se encuentra adjunta (Anexo No.1) al presente documento. Si es un menor de edad, entonces la autorización deberá
	ser firmada por su acudiente.
	</div>
	</td>
</tr>
'.$acompaniantes.'

</table>';
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Ln(1);

/*
$pdf->Ln(2);
$pdf->Rect(0, 0, 90, 7,'F',array(),array(229, 48, 39));
*/

$pdf->SetFont('helvetica', 'R', 9);
$pdf->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(229, 48, 39)));
$pdf->setCellPaddings(17, 0,0, 0);
$pdf->SetFillColor(229, 48, 39);
$pdf->SetTextColor(255,255,255);
$pdf->SetX(0);
$text="1.1 Tipo de acomodación";
$pdf->Cell(90, 5, $text, 1, 1, 'L', 1, 0);
$pdf->Ln();


//$pdf->CheckBox('No', 5, true, array(), array(), 'OK');


$html = $styles_table.'
<table width="100%" cellpadding="5px">
	<tr>
		<td>
			No de habitaciones Sencilla  &nbsp; &nbsp; &nbsp; &nbsp;
			<span>'.trim($regInfo["simple_room"]) . '</span>
		</td>
		<td>
			No de habitaciones Doble    &nbsp; &nbsp; &nbsp; &nbsp;
			<span>'.trim($regInfo["double_room"]) . '</span>
		</td>
		<td>
			No de  habitaciones Triple    &nbsp; &nbsp; &nbsp; &nbsp;
			<span>'.trim($regInfo["triple_room"]) . '</span>
		</td>
	</tr>  
</table>'; 
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Ln(1);



$pdf->SetFont('helvetica', 'R', 9);
$pdf->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(229, 48, 39)));
$pdf->setCellPaddings(17, 0,0, 0);
$pdf->SetFillColor(229, 48, 39);
$pdf->SetTextColor(255,255,255);
$pdf->SetX(0);
$text="1.2. Extensión (Pre o Post)";
$pdf->Cell(90, 5, $text, 1, 1, 'L', 1, 0);
$pdf->Ln();


//$pdf->CheckBox('No', 5, true, array(), array(), 'OK');

$detail = trim($regInfo["extension"]);

$sql_ex = "SELECT extension_name FROM EXTENSIONS 
		WHERE  id='".$detail."'";
$micon->query( $sql_ex );
$detail = $micon->fetchArray();



if($regInfo["extension_detail"]=='yes'){
		$rta1= $ron;
		$rta2= $roff;
	}else{
		$rta1= $roff;
		$rta2= $ron;		
	}

$html = $styles_table.'
<table width="100%" cellpadding="5px">
	<tr>
		<td  width="20%">
				Si   &nbsp; '.$rta1.' &nbsp;    -   No  &nbsp; '.$rta2.'
		</td>
		<td   width="80%">
				Cual?  &nbsp; &nbsp; &nbsp; &nbsp; 
				<span>'.$detail['extension_name'].'</span>
		</td>
	</tr>  
</table>'; 
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Ln(1);



$pdf->SetFont('helvetica', 'R', 9);
$pdf->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(229, 48, 39)));
$pdf->setCellPaddings(17, 0,0, 0);
$pdf->SetFillColor(229, 48, 39);
$pdf->SetTextColor(255,255,255);
$pdf->SetX(0);
$text="2. Indique su forma de pago";
$pdf->Cell(90, 5, $text, 1, 1, 'L', 1, 0);
$pdf->Ln(1);


$pdf->SetFont('helvetica', 'B', 7);
$pdf->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(255, 255, 255)));
$pdf->setCellPaddings(17, 0,0, 0);
$pdf->SetFillColor(255, 255, 255);
$pdf->SetTextColor(0,0,0);
$pdf->SetX(0);
$text="Lee antes las condiciones aplicables a cada forma de pago y marca con una equis (x) tu selección:";
$pdf->Cell(190, 5, $text, 1, 1, 'L', 1, 0);
$pdf->Ln(1);	

$option_show = ''; 

if($regInfo["start_quotes_date"]!='0000-00-00'){
	$date = new DateTime($regInfo["start_quotes_date"]);
	$d1 = $date->format('d/m/y');
}else{
	$d1 = '__/__/__';
}

if($regInfo["end_quotes_date"]!='0000-00-00'){
	$date = new DateTime($regInfo["end_quotes_date"]);
	$d2 = $date->format('d/m/y');
}else{
	$d2 = '__/__/__';
}

$start_end = '
	<tr>
		<td colspan="2"> 
			Fecha de inicio de cobro de la cuota (DD/MM/AA) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span> '.$d1.' </span>
		 </td>
	</tr>
	<tr>
		<td  colspan="2"> 
			Fecha de fin de cobro de la cuota (DD/MM/AA) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;  <span> '.$d2.' </span> 
		</td>
	</tr>';


	if($regInfo["payment"] == 21){
		$opt1 =$ron;
		$opt2 =$roff;
		$opt3 =$roff;
		$money1 = ''.number_format($regInfo["money"]);
		$money2 = '';
		$money3 = '';
	}elseif($regInfo["payment"] == 22){
		$opt2 =$ron;
		$opt1 =$roff;
		$opt3 =$roff;
		$money1 = ' &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; ';
		$money2 = ''.number_format($regInfo["money"]);
		$money3 = '';
		$cuotes1 = $regInfo["quotes"];
		$cuotes2 = '&nbsp;';
	}elseif($regInfo["payment"] == 23){
		$opt3 =$ron;
		$opt2 =$roff;
		$opt1 =$roff;
		$money1 = ' &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; ';;
		$money2 = '';
		$money3 = ''.number_format($regInfo["money"]);
		$cuotes1 = '&nbsp;';
		$cuotes2 = $regInfo["quotes"];
	}

$option_show ='';

/*
switch($regInfo["payment"]){
	case '21':*/
		$option_show .='<tr>
							<td>'.$opt1.' 2.1 Pago de contado hasta el 31/08/2019 (Descuento del 9%)  </td>
							<td> <span>'.$money1.'</span> </td>
						</tr>';
/*	break;
	case '22':*/
		$option_show .='<tr>
							<td>'.$opt2.'  2.2 Pago hasta el 20/12/2019 (Descuento del 5%)</td>
							<td>No Cuotas: &nbsp; <span>'.$cuotes1.'</span> &nbsp;  -    &nbsp; &nbsp;  Valor: &nbsp;  <span>$ '.$money2.' </span> </td>
						</tr>';//.$start_end;
/*	break;
	case '23':*/
		$option_show .='<tr>
							<td>'.$opt3.'  2.3 Pago a cuotas (hasta el 30 abril 2021)</td>
							<td>No Cuotas: &nbsp; <span>'.$cuotes2.'</span> &nbsp;  -    &nbsp; &nbsp; Valor: &nbsp;  <span>$ '.$money3.' </span> </td>	
						</tr>';//.$start_end;
/*	break;
	
}*/


$html = $styles_table.'
<table width="100%" cellpadding="5px">
	
		'.$option_show.
		$start_end.'	
		
</table>'; 
$pdf->writeHTML($html, true, false, true, false, '');





// Page 2 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

$pdf->AddPage();


$html = $styles_table.'
<table width="100%" cellpadding="5px">
	
	<tr>
		<td   style="padding:9px;">
		<span> Condiciones de pago: </span> <br>
		Los pagos de las cuotas se realizarán por medio de la parametrización “pago de cuotas mediante factura”, y el valor se verá reflejado implicito en la factura por el tiempo
		establecido en este formulario
		</td>
	</tr>
		
</table>'; 
$pdf->writeHTML($html, true, false, true, false, '');


$html = '
<table width="100%" cellpadding="7px" style="font-size: 8px; color: #AEAEAE">
	<tr>
		<td colspan="3" style="padding:9px; color: #383838; font-weight: bold;">
			 Los pagos de contado deberán hacerse a una de las siguientes cuentas bancarias corrientes, según corresponda: 
		</td>
	</tr>
	<tr>	
		<td>
			<span style="color: #383838; font-weight: bold;"> Banco Davivienda </span>  N° 4701-6998-5400
		</td>
		<td>
			<span style="color: #383838; font-weight: bold;"> Banco de Bogotá </span> N° 049089071
		</td>
		<td>
			<span style="color: #383838; font-weight: bold;"> BBVA  </span> N° 401.00782.8
		</td>
	</tr>		
</table>'; 
$pdf->writeHTML($html, true, false, true, false, '');


$html = $styles_table2.'
<table width="100%" cellpadding="15px">
	<tr>
		<td style="text-align: justify; line-height:10px;"><br><br>
			<span>2.1</span> Si selecciona la opción de pago de contado, el valor a pagar es de <span>$ '.$money1.' </span>. Debe remitir copia del comprobante de consignación e
			informar que está pagando el valor total de la convención al representante CAPI de la regional y al correo <span>convenciondealiados@terpel.com</span>. Se recomienda no mezclar
			pagos de combustible y convención, realizar las consignaciones por separado.<br><span>Se recibirán pagos hasta el día 31 de Agosto del 2019 para modalidad de contado.</span>
			<br>
		</td>
	</tr>
</table>
<br><br>
<table width="100%" cellpadding="1px" style="font-size: 7.5px; color: #AEAEAE; border: none;">
	<tr>
		<td colspan="3" style="color: #383838; font-weight: bold;">Los pagos de las cuotas se realizarán por medio de la parametrización “pago de cuotas mediante factura”, y el valor lo verá reflejado implicito en la factura por el tiempo
		establecido en este formulario. 
		</td>
	</tr>
</table>
<br><br>
<table width="100%" cellpadding="15px">
	<tr>
		<td style="text-align: justify; line-height:10px; border-bottom: 0.5px solid #dee2e6;"><br><br>
				<span>2.2</span> Si selecciona la forma de pago a cuotas mediante factura, en cada una de las facturas que le emita Organización Terpel S.A. por concepto de despacho de combustible,
				se le cobrará un monto fijo adicional equivalente a <span>$'.$money2.$money3.'</span> que se destinará exclusivamente como abono al pago de la Convención. Si usted
				selecciona esta opción, el cobro por Convención se empezará a reflejar en su facturación a más tardar dentro de los 10 días siguientes a la firma de este formulario o la fecha
				estipulada en el punto 2.2
		</td>
	</tr>
	<tr>
		<td style="text-align: justify; line-height:10px;">
				<span>2.3</span> Si eligío la ópción pago a cuotas mediante factura (hasta 24 cuotas), corte máximo hasta el 30 de Abril del 2021, tenga en cuenta que el número de cuotas
				se verá modificado según la fecha de inscripción.<br><br>
				El valor de la convención siempre se cancelará en su totalidad, por lo anterior tenga presente las siguientes situaciones que se pueden generar en el momento del
				pago con esta modalidad:
				<ul>
					<li>
						<span>Despachos mayores a los pactados: Se entenderá que se cancela más rápido la convención sobre el tiempo estipulado en este formulario. No obstante, usted puede
					solicitar en este caso que el valor de la cuota parametrizada baje.</span> 
					</li>
					<li>
						<span>Despachos menores a los pactados: se reajustará el valor de la cuota pactada sobre el total del saldo faltante para los siguientes despachos, hasta que se cumpla el
					tiempo estipulado pactado en este formulario y el pago total de la convención.</span> 
					</li>
				</ul>
				Con el objetivo de tener un control y seguimiento de los abonos y saldos, los mismos se revisarán y se reajustarán en caso de que se requiera trimestralmente
	
		</td>
	</tr>
</table>
<br><br>
<table width="100%" cellpadding="5px" style="border: 0px solid #fff;">
	
	<tr>
		<td   style="padding:9px; border: 0px solid #fff;">
			Si usted selecciona la forma de pago a cuotas mediante factura, es posible que el cobro que se haga en sus facturas por concepto de pago de Convención llegue a sobrepasar el
			100% del valor del plan elegido. En ese evento, Organización <span>Terpel S.A.</span> durante el mes siguiente del corte final que se establecido en el numeral <span>2.2 ó 2.3</span> se abonará a la cartera
			de combustible del cliente los montos que hayan excedido el valor total a pagar por la convención.<br><br>
			La forma de pago a cuotas mediante factura sólo puede ser seleccionada si suscribe y entrega este formulario <span>antes del 31 de Agosto del 2019</span>
		</td>
	</tr>
		
</table>'; 
$pdf->writeHTML($html, true, false, true, false, '');



// Page 3 ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$pdf->AddPage();
$pdf->Ln(1);
$pdf->SetFont('helvetica', 'R', 9);
$pdf->SetLineStyle(array('width' => 0.2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(229, 48, 39)));
$pdf->setCellPaddings(17, 0,0, 0);
$pdf->SetFillColor(229, 48, 39);
$pdf->SetTextColor(255,255,255);
$pdf->SetX(0);
$text="3. Ten en cuenta";
$pdf->Cell(90, 5, $text, 1, 1, 'L', 1, 0);
$pdf->Ln();

$html = $styles_table.'
<table width="100%" cellpadding="5px" style="border: 0px solid #fff;">
	
	<tr>
		<td   style="padding:9px; border: 0px solid #fff; line-height:13px;">
		<ul>
			<li>El formulario debe estar completamente diligenciado, no se inscribirán a la Convención personas que tengan datos incompletos o con enmendaduras.</li>
			<li>No se recibirán formularios de inscripción que no tengan anexos la autorización de manejo de datos firmada tanto por el cliente como por sus acompañantes. Recuerde
			que es un formato de habeas data por cada persona y si es menor de edad debe ser firmado por el acudiente en nombre del menor. El formato se encuentra en el Anexo
			No. 1.</li>
			<li>Si usted ya firmó y entregó el formulario e inició su plan de pagos y se retira de la Convención NO SE LE DEVOLVERA DINERO; los montos pagados se abonarán a
			obligaciones por compra de combustibles a la Organización Terpel mediante nota crédito.</li>
			<li>Si usted se retira de la convención después del 31 de julio de 2019, tendrá que pagar las multas establecidas, la cual corresponderá al 100% del valor del cupo adquirido por
			persona. El cupo hace referencia al total del paquete seleccionado.</li>
			<li>Las extensiones son responsabilidad de la agencia de viajes. Si usted desea adquirir independientemente alguna extensión, comuníquese directamente con la oficina de
			Gema Tours de su región, de acuerdo con el listado que se encuentra al final del ayuda ventas.</li>
			<li>La ciudad de salida es Bogotá. Los precios de la Convención no incluyen tiquetes al interior de Colombia. Estos tiquetes internos los debe coordinar usted directamente
			con su agencia de viajes. Organización Terpel S.A. no se hace responsable de tiquetes internos.</li>
			<li>Para asistir a la Convención es necesario que usted, y las demás personas que viajarán, cuenten con pasaporte vigente. Usted debe realizar los trámites necesarios
			para obtener oportunamente el pasaporte , ya que estos no están incluidos y no son prestados por Organización Terpel S.A., ni están incluidos dentro de los precios de
			asistencia a la Convención. La agencia de viajes puede guiarle en cuanto a qué trámites debe cumplir. Si usted no asiste a la convención por no tener el pasaporte vigente
			NO SE DEVOLVERA EL DINERO.</li>
			
		</ul>
		</td>
	</tr>
		
</table>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<table width="100%" cellpadding="5px">
<tr>
	<td  width="30%">
		Nombre:
	</td>
	<td width="70%">
		Firma de aceptación<br>	
		de términos y condiciones:	
	</td>
</tr> 
<tr>
	<td  width="30%">
		No. de Cédula
	</td>
	<td   width="70%">
		Fecha de entrega y firma<br>
		de este formulario (DD/MM/AA)
	</td>
</tr>  
</table>'; 
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Ln(1);


// Page 4------ SetPrintHeader ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$pdf->setPrintFooter('');
$pdf->AddPage();

$html = $styles_table.'
<table width="100%" cellpadding="5px" style="border: 0px solid #fff;">
	
<tr>
	<td style="padding:9px; border: 0px solid #fff; font-size: 19px; text-align:center;	">
		<span> Anexo No. 1 </span><br>
	</td>
</tr>
	
<tr>
	<td style="padding:9px; border: 0px solid #fff; font-size: 12px; text-align:center;">
		<span> Anexo Formato Autorización de uso de Imagen y de Datos Personales </span> <br><br>
	</td>
</tr>

	
	<tr>
		<td   style="padding:9px; border: 0px solid #fff; line-height:13px;">
		
		En cumplimiento de las disposiciones de la Ley 1581 de 2012 y su Decreto reglamentario 1377 de 2013, que desarrollan el derecho de habeas data,
		solicitamos su autorización para que Organización Terpel S.A. (en adelante Terpel) pueda recopilar, almacenar, archivar, copiar, analizar, usar y consultar
		los datos e imágenes obtenidos con las siguientes nalidades, todos relacionados con las actividades de Terpel y el ejercicio de su objeto social: a)
		informes institucionales internos y externos, presentaciones de voceros (representantes legales, apoderados, presidente, vicepresidentes y empleados
		de Terpel) frente a diferentes órganos societarios y corporativos o ante autoridades, campañas externas de mercadeo, campañas de comunicaciones
		internas, publicación de documentos institucionales sobre información de la empresa; b) El envío de correspondencia, correos electrónicos o contacto
		telefónico con sus clientes, proveedores y usuarios de sus distintos programas en desarrollo de actividades publicitarias, promocionales, de mercadeo
		(principalmente para planes de delidad y relacionales) de ejecución de ventas, estudios de mercado enfocados a su actividad de distribución de
		combustibles líquidos, lubricantes o GNV o prestación de servicios complementarios; c) Compartirla con terceros aliados, proveedores y sociedades del
		mismo grupo empresarial ubicadas dentro o fuera del país, en particular para la realización de actividades de conocimiento al cliente, relacionamiento
		comercial o publicitario y gestión de ventas. d) Transferencia y transmisión de datos a terceros con quienes realice alianzas relacionadas con su objeto
		social, contrate estudios o les encargue el tratamiento de datos. e) Mantenimiento por sí mismo o a través de un tercero, de las bases de datos; f) Para su
		uso en redes sociales, así como para la elaboración de los informes de gestión de Terpel y su publicación en elementos comerciales o de mercadeo físicos
		o electrónicos elaborados por Terpel o sus proveedores. <br><br>
		Los derechos que le asisten conforme a la ley al Titular de los datos son los siguientes: a) Conocer, actualizar y recticar sus datos personales frente a los
		Responsables del Tratamiento o Encargados del Tratamiento. Este derecho se podrá ejercer, entre otros frente a datos parciales, inexactos, incompletos,
		fraccionados, que induzcan a error, o aquellos cuyo Tratamiento esté expresamente prohibido o no haya sido autorizado; b) Solicitar prueba de la
		autorización otorgada al Responsable del Tratamiento salvo cuando expresamente se exceptúe como requisito para el Tratamiento de conformidad con
		lo previsto en el artículo 10 de la Ley 1581 de 2012; c) Ser informado por el Responsable del Tratamiento o el Encargado del Tratamiento, previa solicitud,
		respecto del uso que le ha dado a sus datos personales; d) Presentar ante la Superintendencia de Industria y Comercio quejas por infracciones a lo
		dispuesto en la presente ley y las demás normas que la modiquen, adicionen o complementen; e) Revocar la autorización y/o solicitar la supresión del dato
		cuando en el Tratamiento no se respeten los principios, derechos y garantías constitucionales y legales. La revocatoria y/o supresión procederá cuando la
		Superintendencia de Industria y Comercio haya determinado que en el Tratamiento el Responsable o Encargado han incurrido en conductas contrarias
		a esta ley y a la Constitución; f) Acceder en forma gratuita a sus datos personales que hayan sido objeto de Tratamiento al menos una vez cada mes
		calendario y cada vez que existan modicaciones sustanciales de las políticas de tratamiento de información que motiven nuevas consultas. g) Presentar
		consultas, peticiones y quejas en torno al manejo dado por la entidad a su información personal y en general al ejercicio del Hábeas Data. h) Para efectos
		de registro, participación y actividades de la Convención Terpel Aliados y posteriores invitaciones a eventos que se realicen a través de correspondencia,
		correos electrónicos o contacto telefónico. <br><br>
		Cuando se recolecten datos personales sensibles, o de niños, niñas o adolescentes, el Titular de los datos no estará obligado a responder las preguntas
		que versen sobre los mismos ni a autorizar su tratamiento.
		La vigencia de la base datos será la del periodo de tiempo en que se mantengan las nalidades del tratamiento en cada base de datos o aquel requerido de
		acuerdo a las normas contractuales, contables, comerciales, tributarias, o cualquiera aplicable según la materia, con un plazo máximo de cincuenta años,
		y prorrogables por períodos iguales según las necesidades de la entidad. Le informamos que puede consultar la Política de tratamiento de la Información
		de Terpel en la página www.terpel.com, que contiene los lineamientos, directrices, y procedimientos sobre el tratamiento de la información de terceros
		por parte de Terpel y la forma de hacer efectivos sus derechos, consultas y solicitudes de supresión de datos. Así mismo podrá consultar allí cualquier
		actualización a dicha política.  <br><br>
		En virtud de la presente aceptación, voluntariamente cede y transere de manera irrevocable a Terpel los derechos patrimoniales de autor sobre cualquier
		imagen y en general cualquier creación de propiedad intelectual que nazca, se desarrolle, se implemente, se produzca, se cree con ocasión de la presente
		aceptación. Esta transferencia comprende todos los derechos patrimoniales respecto de todas las modalidades de explotación de la obra conocidas o
		por conocerse, tiene vigencia por todo el tiempo de protección legal y tiene efectos para el territorio mundial. Esta transferencia deja a salvo los derechos
		morales de autor, los que seguirán en cabeza de quien suscribe el presente documento de manera que por este hecho no se genera ninguna obligación
		pecuniaria a cargo de ORGANIZACIÓN TERPEL S.A






		</td>
	</tr>
		
</table>'; 
$pdf->writeHTML($html, true, false, true, false, '');


$html = $styles_table.'
<br>
<table width="100%" cellpadding="5px">
<tr>
	<td>
		Acepto: &nbsp;  <span>Si</span> '.$coff.' &nbsp;  -    &nbsp; &nbsp;    <span>No </span> '.$coff.' 
	</td>
	<td colspan="2">
		Nombre 
	</td>
  </tr>
<tr>
	<td colspan="2">
		En representación <br>
		de (Nombre del menor)
	</td>
    <td>Firma: </td>
</tr>
</table>
<table width="100%" cellpadding="5px" style="border: 0px solid #fff;">
	<tr>
		<td style="padding:9px; border: 0px solid #fff; text-align: right;">
			Si se trata de un menor de edad debe firmar su padre o madre
		</td>
	</tr>		
</table>
'; 

$pdf->writeHTML($html, true, false, true, false, '');


$date = date('d_m_y__H_m_s_a', time());
$pdf->Output('registration_form_'.$regInfo["document"].'__'.$date.'_.pdf', 'I');

}else{
  header("Location: ../list.php"); 
  exit();
}