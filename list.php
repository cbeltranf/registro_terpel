<?php 
session_start();
require('include/security.php');
require('include/utils.php');
require('include/mysql_class.php');
require_once 'vendor/autoload.php';
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);   
error_reporting(E_ALL);
*/
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <title>Dashboard · Terpel</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="icon" href="images/icons/favicon.ico" type="image/x-icon">
  <meta property="og:type" content="website"/>
  <meta property="og:image" content="https://convenciondealiadosterpel.com/prereg/icons/apple-icon-180x180.png"/>
  <meta property="og:url" content="https://convenciondealiadosterpel.com/prereg"/>
  <meta property="og:title" content="Registro · Terpel"/>
  
  <?php include_once("analyticstracking.php") ?>
 <link rel="apple-touch-icon" sizes="57x57" href="icons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="icons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="icons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="icons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="icons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="icons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="icons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="icons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="icons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="icons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/icons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="images/icons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/icons/favicon-16x16.png">
<link rel="manifest" href="images/icons/manifest.json">
<meta name="msapplication-TileColor" content="#C30B13">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#C30B13">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/all.min.css">
  <link rel="stylesheet" href="css/terpel-fonts.css">
  <link rel="stylesheet" href="css/inmov.css">
  <link rel="stylesheet" href="css/animate.css">
  <link rel="stylesheet" href="js/datatable/datatables.min.css">
  <link rel="stylesheet" href="js/datatable/Responsive-2.2.2/css/responsive.dataTables.css">

<style type="text/css">
  
    body {    
        background-color: white !important;   
     } 
</style>
</head>
<body>
<div class="pageLoader"></div>
<?php
    $selected_menu = 'registro'; 
    include_once("header.php") ?>

<div class="container">
    

     <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4 pl-0">
        <h1 class="page-title" ><a class="text-danger pr-1" href="dashboard.php"><i class="fa-fw fa fa-home"></i> Inicio</a> · <span class="pl-1">  Formularios y Operadores</span></h1>
    </div>

    <div class="row">
        <div class="col-lg-12 col-xs-12">


            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
            <?php /*?>
            <li class="nav-item">
                <a class="nav-link active text-danger" data-toggle="tab" href="#home" id="tab_pre">Pre Registros</a>
            </li> <?php */?>
            <li class="nav-item">
                <a class="nav-link active text-danger" data-toggle="tab" href="#menu1" id="tab_reg">Formularios</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-danger" data-toggle="tab" href="#menu2" id="tab_no_reg">Operadores</a>
            </li>

            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
            <?php /*?>
                <div class="tab-pane container active" id="home">
                        <table id="prereg" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Documento</th>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Email</th>
                                    <th>Celular</th>
                                    <th class="text-center">Acompañantes</th>
                                    <th  class="text-center">Respuesta</th>
                                    <th>Habeas Data</th>
                                    <th>Codigo EDS</th>
                                    <th class="none">Nombre EDS </th>
                                    <?php ///?><th class="control text-center">Acciones</th><?php ?>
                                </tr>
                            </thead>    
                            <tbody>
                            <?php   $sql = "SELECT  pre.id,  pre.USER_id AS document, pre.`first_name`, pre.`last_name`, pre.`email`, pre.`mobile`,
                                        pre.`companions`, pre.`response`, pre.`heabeas_accept_date`, 
                                        g.`name` AS gas, g.`id` AS gas_id
                                    FROM `GASSTATIONREG` AS pre
                                        INNER JOIN `GASSTATION` AS g ON pre.`GASSTATION_id` = g.`id`
                                    WHERE g.`ZONELEADER_id` =  '".$_SESSION["id"]."' ";
                            $micon->query($sql);
                            while($item = $micon->fetchArray())
                            { ?>
                                <tr id='row-<?php echo $item['document'];?>'>
                                    <td><?php echo $item['document']?></td>
                                    <td><?php echo $item['first_name']?></td>
                                    <td><?php echo $item['last_name']?></td>
                                    <td><?php echo $item['email']?></td>
                                    <td><?php echo $item['mobile']?></td>
                                    <td  class="text-center"><?php echo $item['companions']?></td>
                                    <td  class="text-center"><?php echo $item['response']?></td>
                                    <td style="white-space:nowrap"><?php echo $item['heabeas_accept_date']?></td>
                                    <td><?php echo $item['gas_id']?></td>
                                    <td  class="none"><?php echo $item['gas']?></td>
                                    <?php //?><td  class="control text-center"> <a href="#" data-id='<?php echo $item['document'] ?>' onclick="javascript:void(0)" class='text-danger'><i class="fas fa-eye "></i></a>  </td><?php /* show.php?id=<?php echo $item['document'] ?> ?> 
                                </tr>
                            <?php }?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Documento</th>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Email</th>
                                    <th>Celular</th>
                                    <th class="text-center">Acompañantes</th>
                                    <th  class="text-center">Respuesta</th>
                                    <th>Habeas Data</th>
                                    <th>Codigo EDS</th>
                                    <th  class="none">Nombre EDS </th>
                                    <?php //?><th  class="control text-center">Acciones</th><?php ?>
                                </tr>
                            </tfoot>
                        </table>
                </div>
                <?php */?>

                
                <div class="tab-pane container active " id="menu1">

                            <table id="reg"  class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th  data-class="expand">Documento</th>
                                        <th class="control">Nombre Completo</th>
                                        <th class="none">Cumpleaños</th>
                                        <th class="none">Email</th> 
                                        <th class="none">Celular</th>
                                        <th class="control">Acompañantes</th>
                                        <th class="control">Estado</th>
                                        <th class="control">EDS Id</th>
                                        <th class="none">Nombre EDS </th>
                                        <th class="none">Region </th>
                                        <th class="none">Ciudad </th>
                                        <th class="control">Empresa </th> 
                                        <th class="control">NIT </th>  
                                        <th class="none">Pasaporte </th>
                                        <th class="control">Contrato Firmado </th>
                                        <th class="control text-center">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php   
                                    $sq2 = "SELECT r.`document`, g.`id` AS gas_id , g.name AS gas, regional,/*rg.`name` AS region, */
                                                    r.`name`, r.`last_name`, r.`birthday`, r.`email`, r.`city`, r.`bussiness`, 
                                                    r.`nit`, r.`eds_name`, r.`sap_code`, r.`unique_code`, r.`adress`, r.`phone`, 
                                                    r.`cellphone`, r.`companions`, r.`simple_room`, r.`double_room`, r.`triple_room`, 
                                                    r.`extension`, r.`extension_detail`, r.`passport_number`, r.`signed_contract`, r.status_reg
                                            FROM `registration` AS r
                                                    INNER JOIN `GASSTATION` AS g ON r.`gasstation_id` = g.`id`
                                            WHERE g.`ZONELEADER_id` =  '".$_SESSION["id"]."' order by r.name asc";
                                                   /* INNER JOIN `REGIONAL` AS rg ON rg.`id` = r.`regional_id`*/

                                $micon->query($sq2);
                                while($item = $micon->fetchArray())
                                {
                                    switch ($item['status_reg']){
                                        case 'ns':
                                            $estado = '<span  class="text-danger font-weight-bold">Pre-registrado</span>';
                                        break;
                                        case 'd':
                                            $estado = '<span class="text-primary font-weight-bold">Borrador</span>';
                                        break;
                                        case 'f': 
                                            $estado = '<span class="text-primary font-weight-bold">Finalizado</span>';
                                        break;
                                        default: //c
                                            $estado = '<span class="text-success font-weight-bold">Completo</span>';
                                        break;
                                    }
                                ?>
                                <tr>
                                        <td class="nowrap"><?php echo $item['document'] ?> </td>
                                        <td><?php echo $item['name'].' '.$item['last_name'] ?></td>
                                        <td><?php echo $item['birthday'] ?></td>
                                        <td><?php echo $item['email'] ?></td> 
                                        <td><?php echo $item['cellphone'] ?></td>
                                        <td class="text-center"><?php echo $item['companions'] ?></td>
                                        <td><?php echo $estado ?></td>
                                        <td><?php echo $item['gas_id'] ?></td>
                                        <td  class="none"><?php echo $item['gas'] ?></td>
                                        <td><?php echo $item['regional'] ?></td>
                                        <td><?php echo $item['city'] ?></td>
                                        <td><?php echo $item['bussiness'] ?></td>
                                        <td><a target="_blank" class="text-secondary" href="../prereg/php/autoLogin.php?id=<?php echo $item['nit'] ?>"><?php echo $item['nit'] ?> <i class="fal fa-external-link"></i></a> </td> 
                                        <td><?php echo ($item['passport'])?'<a href="'.$item['passport'].'" target="_blank">Download</a>':'No file' ?></td>
                                        <td><?php echo ($item['signed_contract'])?'<a href="'.$item['signed_contract'].'" target="_blank">Download</a>':'No file' ?></td>
                                <td  class="control text-center"> 

                                    <?php if($item['status_reg'] == 'd' || $item['status_reg'] == 'ns'){?> 
                                        <a href="registration.php?id=<?php echo $item['document'] ?>" class='text-danger' data-toggle="tooltip1" title="Editar formulario">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                    <?php } ?> 

                                    <?php if($item['status_reg'] == 'c'){?> 
                                        <a target="_blank" href="php/pdf_report.php?id=<?php echo $item['document'] ?>"  class='text-danger' title="Generar PDF" ><i class="fas fa-file-pdf"></i></a> 

                                         <a href="registrationResume.php?id=<?php echo $item['document'] ?>" class='text-danger' title="Visualizar información"><i class="fas fa-eye "></i> </a> 
                                            
                                        <a href="registrationResume.php?id=<?php echo $item['document'] ?>&a=upload"  class='text-danger' title="Subir formulario firmado" ><i class="fas fa-upload"></i><a> 
                                    <?php } ?> 

                                    <?php if($item['status_reg'] == 'f'){?> 
                                        <a href="registration.php?id=<?php echo $item['document'] ?>" class='text-danger'>
                                            <i class="fas fa-eye "></i>
                                        </a>
                                    <?php } ?> 

                                    
                                </td>
                                    </tr>
                                <?php }?>
                                </tbody>
                                
                            </table>
                </div>



                
                <div class="tab-pane container fade " id="menu2">

                            <table id="noreg"  class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th  >Documento</th>
                                        <th>Nombre</th>
                                       <!--  <th>Email</th>
                                       <th>Celular</th>  -->
                                        <th class="text-center">Último Inicio de Sesión </th>
                                        <th class="text-center">Canitdad de EDS</th>
                                        <th class="text-center">Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php   
                                    $sq2 = " SELECT
                                        `USER`.`id`
                                        , `USER`.`first_name`
                                        , `USER`.`last_login_date`
                                        , (SELECT COUNT(*)  FROM  `GASSTATIONREG` WHERE `GASSTATION`.USER_id = `GASSTATIONREG`.USER_id) AS nPre
                                        , (SELECT COUNT(*)  FROM  `GASSTATION` WHERE `GASSTATION`.USER_id = `USER`.`id` AND `GASSTATION`.`ZONELEADER_id` =  '".$_SESSION[id]."') AS nTotal
                                        #, COUNT(GASSTATION.id) AS gasstation
                                        FROM
                                        `USER`
                                        INNER JOIN  `GASSTATION`
                                        ON (`GASSTATION`.`USER_id` = `USER`.`id`)
                                           
                                            WHERE `ZONELEADER_id` =  '".$_SESSION[id]."' 
                                            GROUP BY id
                                        ORDER BY `first_name` ASC ";
//echo $_SESSION["id"];
                                $micon->query($sq2);
                                while($item = $micon->fetchArray())
                                {
                                ?>
                                    <tr>
                                        <td>
                                            <?php echo $item['id'] ?> 

                                           
                                        </td>
                                        <td><?php echo $item['first_name'] ?></td>
                                        <!-- <td><?php echo $item['email'] ?></td>
                                        <td><?php echo $item['mobile'] ?></td> -->
                                        <td><?php echo($item['last_login_date']!="") ? friendlyDate( $item['last_login_date'], false, false, true ) : ""   ?></td> 
                                        <td class="text-center">
                                            
                                           <!--  <div class="progress" style="height: 1px;">
                                             <div class="progress-bar text-center text-dark" role="progressbar" style="width: <?=($item['nPre']/$item['nTotal'])*100?>%;" aria-valuenow="<?=($item['nPre']/$item['nTotal'])*100    ?>" aria-valuemin="0" aria-valuemax="100"><?php echo $item['nPre']  . ' de ' . $item['nTotal'] ?></div>
                                           </div> -->
                                           <?
                                                $percent = round(($item['nPre']/$item['nTotal'])*100, 2)
                                            ?>
                                                <span class=""><?php echo $item['nPre']  . ' de ' . $item['nTotal'] ?> <span class="text-muted">(<?=$percent;?>%)</span></span>
                                            <div class="progress" style="height: 2px;">
                                                <div class="progress-bar text-center text-dark" role="progressbar" aria-valuenow="<?=($item['nPre']/$item['nTotal'])*100?>" aria-valuemin="0" aria-valuemax="100" style="width:<?=($item['nPre']/$item['nTotal'])*100?>%">
                                                  
                                                </div>
                                              </div>

                                        </td>
                                        <td nowrap="">
                                            <? if ($percent!=100) { ?>

                                            <a  target="_blank" class="text-secondary" href="../prereg/php/autoLogin.php?id=$item[id]">Pre-registrar <i class="fal fa-external-link"></i></a>
                                            <? }
                                            else{ ?>

                                                <span class="text-muted">Completado</span>
                                           <? } ?>


                                           
                                        </td>
                                    </tr>
                                <?php }?>
                                </tbody>
                                
                            </table>
                </div>

            </div>
    
        </div>
    </div>
</div>
    
<script src="js/jquery-3.3.1.js"></script>  
<script src="js/jquery.validate.js"></script>
<script src="js/messages_es.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/terpel-jquery.js"></script>
<script src="js/datatable/datatables.min.js"></script>
<script src="js/datatable/Responsive-2.2.2/js/responsive.bootstrap4.js"></script>
<script src="js/bootstrap.min.js"></script>


<script>
$(document).ready(function() {
    $('#prereg, #noreg').DataTable({
       responsive: true,
       "pageLength": 50,
       dom: 'Bfrtip',
       "order": [[1, 'asc']],
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
       language: {
	            "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(Filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
        }
       
   });

   $('#reg').DataTable({
       responsive: true,
       dom: 'Bfrtip',
       "pageLength": 50,
      "order": [[1, 'asc']],
       
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],
       language: {
	            "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
        }
       
   });

   $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
       $(".dataTables_filter input[type=\'search\']").val("").keyup();
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust()
           .responsive.recalc();
    });   

});


   $(window).on("load",function() {

        $(".pageLoader").fadeOut("fast");

            setTimeout(function(){
                <?php
                        if(isset($_GET['reg']) && !empty($_GET['reg'])){
                           
                            echo '$("#tab_reg").click();';

                            switch ($_GET['reg']){
                                case 'ns':
                                    echo '$(".dataTables_filter input[type=\'search\']").val("Pre-registrado").keyup();';
                                break;
                                case 'd':
                                    echo '$(".dataTables_filter input[type=\'search\']").val("Borrador").keyup();';
                                break;
                                case 'f': 
                                    echo '$(".dataTables_filter input[type=\'search\']").val("Finalizado").keyup();';
                                break;
                                default: //c
                                    echo '$(".dataTables_filter input[type=\'search\']").val("Completo").keyup();';
                                break;
                            }
                            
                            echo '$(".dataTables_filter input[type=\'search\']").focus();';
                        }
                ?>
            }, 500);
    });


</script>

    </body>    
</html>