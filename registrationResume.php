<?
session_start();
require('include/security.php');
require('include/utils.php');
require('include/mysql_class.php');


$id = satinize_me($_GET['id'], 'int');  


$sql = "SELECT
    *
FROM
    `registration`
    WHERE document='$id'";

$micon->query( $sql );
$regInfo = $micon->fetchArray();


$sql = "SELECT
    *
FROM
    `companions`
    WHERE registration_id='$id'";

$micon->query( $sql );

$dataCompa = array();


while($regCompa = $micon->fetchArray()){
    $objCompa                  = new stdClass();
    $objCompa->name            = $regCompa[ name ];
    $objCompa->type            = $regCompa[ type ];
    $objCompa->age             = $regCompa[ age ];
    $objCompa->passport_number = $regCompa[ passport_number ];
    $objCompa->have_passport   = $regCompa[ have_passport ];
    $objCompa->passport_image  = $regCompa[ passport_image ];

    array_push( $dataCompa, $objCompa );
}
//echo $dataCompa[0]->name;
/*

Array
(
    [0] => stdClass Object
        (
            [name] => Carolina
            [type] => Adulto
            [age] => 
            [passport_number] => 
            [have_passport] => N
            [passport_image] => 
        )

    [1] => stdClass Object
        (
            [name] => Flow
            [type] => Adulto
            [age] => 
            [passport_number] => 
            [have_passport] => N
            [passport_image] => 
        )

)
*/
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <title>Registro de EDS · Terpel</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="icon" href="images/icons/favicon.ico" type="image/x-icon">
  <meta property="og:type" content="website"/>
  <meta property="og:image" content="https://convenciondealiadosterpel.com/prereg/icons/apple-icon-180x180.png"/>
  <meta property="og:url" content="https://convenciondealiadosterpel.com/prereg"/>
  <meta property="og:title" content="Registro · Terpel"/>
  
  <?php include_once("analyticstracking.php") ?>
 <link rel="apple-touch-icon" sizes="57x57" href="icons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="icons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="icons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="icons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="icons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="icons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="icons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="icons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="icons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="icons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/icons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="images/icons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/icons/favicon-16x16.png">
<link rel="manifest" href="images/icons/manifest.json">
<meta name="msapplication-TileColor" content="#C30B13">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#C30B13">



  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/all.min.css">
  <link rel="stylesheet" href="css/terpel-fonts.css">
  <link rel="stylesheet" href="css/inmov.css">
  <link rel="stylesheet" href="css/animate.css">


<style type="text/css">
  
    body {    
      background-color: white !important;   
     } 
  

</style>

</head>
<body>

  

 <div class="pageLoader"></div>

<?php 
$selected_menu = 'registro'; 
include_once("header.php") 
?>

<div class="container">
<div class="row d-print-none">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4 pl-0">
        <h1 class="page-title" ><a class="text-danger pr-1" href="dashboard.php"><i class="fa-fw fa fa-home"></i> Inicio</a> · <span class="pl-1">  <a class="text-danger px-1" href="list.php">Registros </a></span> · <span class="pl-1">Resumen del registro </span> · 
          <?
          switch ($regInfo['status_reg']){
              case 'ns':
                  $estado = '<span  class="badge badge-danger tt_normsbold">Pre-registrado</span>';
              break;
              case 'd':
                  $estado = '<span class="badge badge-primary tt_normsbold">Borrador</span>';
              break;
              case 'f': 
                  $estado = '<span class="badge badge-success tt_normsbold">Finalizado</span>';
              break;
              default: //c
                  $estado = '<span class="badge badge-success tt_normsbold">Completo</span>';
              break;
          }
          echo $estado;
?>
        </h1>
    </div>
</div>




 



    <form method="post" name="frmUploadForm" id="frmUploadForm"  class="w-100  "  >

<div class="row my-3">

                                            
  <? if (!$_GET[a]=='upload'){ ?>
   <div class="col">


        <a target="_blank" href="php/pdf_report.php?id=<?=$id?>" data-wow-delay="0.60s" class="btn  btn-lg btn-block   btn-warning tt_normslight  wow  fadeIn" t><i class="fal fa-file-pdf"></i> Ver pdf </a>

      </div>
    <? } ?>
  <div class="col" >   
      <!-- <a href="#" data-wow-delay="0.70s" id="btnSave" class="btn  btn-lg btn-block   btn-warning tt_normslight  wow  fadeIn" ><i class="fal fa-upload"></i> Subir formulario firmado (*.pdf) </a> -->

    <!-- <label title="Subir pasaporte" id="label-file-passPort0" for="file-passPort0"   class="btn btn btn-secondary " style="margin-bottom: 0;">
                         <i class="fal fa-upload"></i></label>
                        <input type="file" accept="image/jpeg,application/pdf"  name="file-passPort0" id="file-passPort0" class="d-none btnUpload"> -->

                        <label title="Subir pasaporte" id="label-file-upload" for="file-upload"   class="btn  btn-lg btn-block   btn-warning tt_normslight  wow  fadeIn" style="margin-bottom: 0;">
    <i class="fal fa-upload"></i> Subir formulario firmado (*.pdf)</label>
      <input type="file" accept="image/jpeg,application/pdf"  name="file-upload" id="file-upload" class="d-none btnUpload">

    </div>


  </div>


<div class="card wow animated fadeIn" style="width: 100%;" data-wow-delay="0.20s">  
  <div class="card-header card-header text-white bg-secondary">
  
<h5 class="m-0 p-0 tt_normsbold">1. Información general</h5>
</div>
    

   <div class="card-body card-body p-0">

         
            <ul class="list-group list-group-flush">
            <li class="list-group-item">

            <div class="form-row">
                        <div class="form-group col-md-8">
                            <label for="txtFullName" class="tt_normsmedium">Nombres y Apellidos Completos <small style="display: inherit;" id="txtFullNameHelp" class="form-text text-muted">
                            Como figura en el pasaporte
                          </small></label>
                            <input disabled id="txtFullName" required type="text" class="form-control input-lg tt_normslight"  name="txtFullName" aria-describedby="txtFullNameHelp" minlength="5" value="<?=$regInfo[name] . " " . $regInfo[last_name] ?>"  autocomplete="off" >

                            

                        </div>


                      <div class="form-group col-md-4">
                            <label for="txtDoc" class="tt_normsmedium">Número de Cédula</label>
                            <input id="txtDoc" disabled type="number" value="<?=$regInfo[document] ?>" class="disabled form-control input-lg tt_normslight"  name="txtDoc" minlength="5"  autocomplete="off" >
                        </div>

                      </div>
						<input id="hDoc"  type="hidden" value="<?=$regInfo[document] ?>"   name="hDoc" minlength="5"  autocomplete="off" >

                      <div class="form-row">

                        <div class="form-group col-md-4  ">
                            <label for="rdoHasPassPort0" class="tt_normsmedium">¿Tiene pasaporte?</label>

                            <div class="c-inputs-stacked p-0 pt-2">
                            <label class="c-input c-radio pr-3 ">
                            <input class="required" disabled   autocomplete="off" name="rdoHasPassPort0" type="radio" value="yes" <?=($regInfo[has_passport]=='yes') ? "checked" : "" ?> >
                            <span class="c-indicator pl-2"></span> Sí </label>
                          
                            <label class="c-input c-radio">
                            <input class="required" disabled autocomplete="off"  name="rdoHasPassPort0" type="radio" value="no" <?=($regInfo[has_passport]=='no') ? "checked" : "" ?>>
                            <span class="c-indicator pl-2"></span> No </label>
                          
                          </div>
                        </div>


                        <div class="form-group col-md-4">
                           <label for="txtPassport0" class="tt_normsmedium">No. Pasaporte</label>
                            <input disabled autocomplete="off" <?=($regInfo[has_passport]=='yes') ? "required" : "" ?>  id="txtPassport0" type="text" class="form-control input-lg tt_normslight "  name="txtPassport0" <?=($regInfo[has_passport]=='no') ? "disabled" : "" ?>  value="<?=$regInfo[passport_number] ?>"  >

                        </div>    

                        <div class="form-group col-md-4 pt-4">
                            
                        
                        <a  target="_blank" href="<?=($regInfo[passport_file]!="") ? URL . "travel_docs/" . $regInfo[passport_file] : "#" ?>" class="btn btn-success <?=($regInfo[passport_file]=="") ? "disabled" : "" ?>" id="view-file-0" >
                        <i class="fal fa-eye"></i></a> 
                            

                        </div>                    

                      </div>


                      <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="txtBDay" class="tt_normsmedium">Fecha de nacimiento</label>
                            <input disabled required id="txtBDay" type="date" class="form-control input-lg tt_normslight" value="<?=$regInfo[birthday] ?>"  name="txtBDay"  >

                        </div>
                      <div class="form-group col-md-4">
                            <label for="txtEMail" class="tt_normsmedium">Correo electrónico</label>
                            <input disabled required id="txtEMail" type="email" class="form-control input-lg tt_normslight"  value="<?=$regInfo[email] ?>"  name="txtEMail"  >

                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="txtCity" class="tt_normsmedium">Ciudad</label>
                            <input disabled minlength="5" required id="txtCity" type="text" class="form-control input-lg tt_normslight" value="<?=$regInfo[city] ?>"  name="txtCity"  >

                        </div>

                      </div>


                      <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="txtBussiness" class="tt_normsmedium">Razón social</label>
                            <input disabled minlength="5" required id="txtBussiness" type="text" class="form-control input-lg tt_normslight"  value="<?=$regInfo[bussiness] ?>"  name="txtBussiness"  >

                        </div>
                      <div class="form-group col-md-4">
                            <label for="txtNit" class="tt_normsmedium">Nit</label>
                            <input disabled id="txtNit" type="number" class="form-control input-lg tt_normslight"  value="<?=$regInfo[nit] ?>" name="txtNit" >

                        </div>
                         
                        <div class="form-group col-md-4">
                            <label for="txtEDS" class="tt_normsmedium">Nombre Completo EDS</label>
                            <input disabled minlength="5" required id="txtEDS" type="text" class="form-control input-lg tt_normslight" value="<?=$regInfo[eds_name] ?>" name="txtEDS" >

                        </div>

                      </div>




                      <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="txtRegionalName" class="tt_normsmedium">Regional</label>
                            <input disabled minlength="5" required id="txtRegionalName" type="text" class="form-control input-lg tt_normslight" value="<?=$regInfo[regional] ?>" name="txtRegionalName" >

                        </div>
                      <div class="form-group col-md-4">
                            <label for="txtSapCode" class="tt_normsmedium">Código SAP</label>
                            <input disabled minlength="3" required id="txtSapCode" type="text" class="form-control input-lg tt_normslight" value="<?=$regInfo[sap_code] ?>" name="txtSapCode"  >

                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="txtEDS" class="tt_normsmedium">Código Único</label>
                            <input disabled minlength="3" required id="txtUniqueCode" type="text" class="form-control input-lg tt_normslight" value="<?=$regInfo[unique_code] ?>" name="txtUniqueCode"  >

                        </div>

                      </div>
                    


                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="txtAdress" class="tt_normsmedium">Dirección</label>
                            <input disabled required id="txtAdress" type="text" class="form-control input-lg tt_normslight" value="<?=$regInfo[adress] ?>"  name="txtAdress"  >

                        </div>
                      <div class="form-group col-md-4">
                            <label for="txtMobilePhone" class="tt_normsmedium">Número de Celular</label>
                            <input disabled minlength="10" required id="txtMobilePhone" type="tel" class="form-control input-lg tt_normslight" value="<?=$regInfo[cellphone] ?>"  name="txtMobilePhone" >

                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="txtLandPhone" class="tt_normsmedium">Teléfono Fijo</label>
                            <input disabled minlength="7" required id="txtLandPhone" type="tel" class="form-control input-lg tt_normslight"  value="<?=$regInfo[phone] ?>" name="txtLandPhone"  >

                        </div>

                      </div>


          </li>


            <li class="list-group-item passport-space " id="liInfoCompanion"  >
              
              


            </li>
            <li class="list-group-item passport-space " id="liCompanion1" style=" border-top: 0; padding-top: 0;">

                <div class="form-row">

                        <div class="form-group col-md-4  ">
                            <label for="rdoCompanion"  class="tt_normsmedium">Cantidad de acompañantes</label>

                            <div class="c-inputs-stacked p-0 pt-2">
                            <label class="c-input c-radio pr-3 ">
                            <input disabled class="required"  autocomplete="off" <?=($regInfo[companions]=='0') ? "checked" : "" ?>  name="rdoCompanion" type="radio" value="0" >
                            <span class="c-indicator pl-2"></span> Ninguno  </label>
                          
                            <label class="c-input c-radio pr-3">
                            <input disabled class="required" autocomplete="off"  <?=($regInfo[companions]=='1') ? "checked" : "" ?> name="rdoCompanion" type="radio" value="1">
                            <span class="c-indicator pl-2"></span> 1 </label>

                            <label class="c-input c-radio pr-3">
                            <input disabled class="required" autocomplete="off"  <?=($regInfo[companions]=='2') ? "checked" : "" ?> name="rdoCompanion" type="radio" value="2">
                            <span class="c-indicator pl-2"></span> 2 </label>
                          
                          </div>
                        </div>
                </div>

                <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="txtCompanion1"  class="tt_normsmedium">Nombre Completo acompañante No 1 </label>
                            <input disabled minlength="5"  id="txtCompanion1" value="<?=$dataCompa[0]->name ?>" type="text" class="form-control input-lg tt_normslight"  name="txtCompanion1" aria-describedby="txtFullNameHelp" minlength="5"  autocomplete="off" >                           

                        </div>


                      

                      </div>




                      <div class="form-row">

                        <div class="form-group col-md-4  ">
                            <label for="rdoHasPassPort1"  class="tt_normsmedium">¿Tiene pasaporte?</label>

                            <div class="c-inputs-stacked p-0 pt-2">
                            <label class="c-input c-radio pr-3 ">
                            <input  disabled class=""  autocomplete="off" <?=($dataCompa[0]->have_passport=='S') ? "checked" : "" ?>  name="rdoHasPassPort1" type="radio" value="S" >
                            <span class="c-indicator pl-2"></span> Sí </label>
                          
                            <label class="c-input c-radio">
                            <input disabled class="" autocomplete="off"  <?=($dataCompa[0]->have_passport=='N') ? "checked" : "" ?> name="rdoHasPassPort1" type="radio" value="N">
                            <span class="c-indicator pl-2"></span> No </label>
                          
                          </div>
                        </div>


                        <div class="form-group col-md-4 ">
                           <label for="txtPassport1"  class="tt_normsmedium">No. Pasaporte</label>
                            <input disabled minlength="5" disabled id="txtPassport1"  value="<?=$dataCompa[0]->passport_number ?>" type="text" class="form-control input-lg tt_normslight"  name="txtPassport1"  >


                            

                        </div>  


                        <div class="form-group col-md-4 pt-4">
                            
                        

                        <a target="_blank" href="<?=($dataCompa[0]->passport_image!="") ? URL . "travel_docs/" . $dataCompa[0]->passport_image : "#" ?>" class="btn btn-success <?=($dataCompa[0]->passport_image=="") ? "disabled" : "required" ?>" id="view-file-1">
                        <i class="fal fa-eye"></i></a> 
                            

                        </div>  




                      </div>

                      <div class="form-row">

                        <div class="form-group col-md-4  ">
                            <label for="rdoAgeRange1" class="tt_normsmedium">¿Es adulto o menor de edad?</label>
                            <div class="c-inputs-stacked p-0 pt-2">
                            <label class="c-input c-radio pr-3 ">
                            <input disabled class="" <?=($dataCompa[0]->type=='Adulto') ? "checked" : "" ?>  autocomplete="off" id="si" name="rdoAgeRange1" type="radio" value="Adulto" >
                            <span class="c-indicator pl-2"></span> Adulto </label>
                          
                            <label class="c-input c-radio">
                            <input disabled class="" <?=($dataCompa[0]->type=='Menor') ? "checked" : "" ?>  autocomplete="off" id="no" name="rdoAgeRange1" type="radio" value="Menor">
                            <span class="c-indicator pl-2"></span> Menor de edad </label>
                          
                          </div>

                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="txtAge1" class="tt_normsmedium">Si es menor de edad especifique la edad</label>
                            <input disabled id="txtAge1" <?=($dataCompa[0]->type=='Adulto') ? "disabled" : "required" ?> value="<?=($dataCompa[0]->age != 0) ? $dataCompa[0]->age : '' ?>" type="number" min="0" max="17" class="form-control input-lg tt_normslight"  name="txtAge1"  >

                        </div>

                         

                      </div>
                      
                     
            </li>

             <li class="list-group-item passport-space " id="liCompanion2">
                  <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="txtCompanion2" class="tt_normsmedium">Nombre Completo acompañante No 2 <small style="display: inherit;" id="txtFullNameHelp" class="form-text text-muted">
                            Deja en blanco para no incluir
                          </small></label></label>
                            <input disabled id="txtCompanion2" value="<?=$dataCompa[1]->name ?>"  type="text" class="form-control input-lg tt_normslight"  name="txtCompanion2" aria-describedby="txtFullNameHelp" minlength="5"  autocomplete="off" >

                           

                        </div>


                      

                      </div>




                      <div class="form-row">

                        <div class="form-group col-md-4  ">
                            <label for="rdoHasPassPort2" class="tt_normsmedium">¿Tiene pasaporte?</label>

                            <div class="c-inputs-stacked p-0 pt-2">
                            <label class="c-input c-radio pr-3 ">
                            <input disabled class=" "  autocomplete="off" <?=($dataCompa[1]->have_passport=='S') ? "checked" : "" ?>  name="rdoHasPassPort2" type="radio" value="S" >
                            <span class="c-indicator pl-2"></span> Sí </label>
                          
                            <label class="c-input c-radio">
                            <input disabled class="" autocomplete="off"  <?=($dataCompa[1]->have_passport=='N') ? "checked" : "" ?> name="rdoHasPassPort2" type="radio" value="N">
                            <span class="c-indicator pl-2"></span> No </label>
                          
                          </div>
                        </div>


                        <div class="form-group col-md-4">
                           <label for="txtPassport2" class="tt_normsmedium">No. Pasaporte</label>
                            <input disabled id="txtPassport2" type="text" class="form-control input-lg tt_normslight" <?=($dataCompa[1]->have_passport=='N') ? "disabled" : "required" ?> value="<?=$dataCompa[1]->passport_number ?>"  name="txtPassport2"  >

                        </div>    

                        <div class="form-group col-md-4 pt-4">
                            
                        

                        <a <?=($regInfo[companions]!='2' ) ? "disabled" : "required" ?> target="_blank" href="<?=($dataCompa[1]->passport_image!="") ? URL . "travel_docs/" . $dataCompa[1]->passport_image : "#" ?>" id="view-file-2" class="btn btn-success <?=($dataCompa[1]->passport_image=="") ? "disabled" : "required" ?>" id="view-file-2">
                        <i class="fal fa-eye"></i></a> 
                            

                        </div>                    

                      </div>

                      <div class="form-row">

                        <div class="form-group col-md-4  ">
                            <label for="rdoAgeRange2" class="tt_normsmedium">¿Es adulto o menor de edad?</label>
                            <div class="c-inputs-stacked p-0 pt-2">
                            <label class="c-input c-radio pr-3 ">
                            <input disabled class="" <?=($dataCompa[1]->type=='Adulto') ? "checked" : "" ?> autocomplete="off" id="si" name="rdoAgeRange2" type="radio" value="Adulto" >
                            <span class="c-indicator pl-2"></span> Adulto </label>
                          
                            <label class="c-input c-radio">
                            <input disabled class="" <?=($dataCompa[1]->type=='Menor') ? "checked" : "" ?> autocomplete="off" id="no" name="rdoAgeRange2" type="radio" value="Menor">
                            <span class="c-indicator pl-2"></span> Menor de edad </label>
                          
                          </div>

                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="txtAge2" class="tt_normsmedium">Si es menor de edad especifique la edad</label>
                            <input disabled id="txtAge2"  value="<?=($dataCompa[1]->age != 0) ? $dataCompa[1]->age : '' ?>" min="0" max="17" type="number" class="form-control input-lg tt_normslight"  name="txtAge2"  >

                        </div>

                      </div>
             </li>


          </ul>

                      


                       
             

              



              

      





  </div>
    <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
  </div>

<!--------------------------------------->

<div class="card mt-3 wow animated fadeIn" style="width: 100%;" data-wow-delay="0.30s">  
  <div class="card-header card-header text-white bg-secondary">
  
<h5 class="m-0 p-0 tt_normsbold">1.1 Tipo de acomodación</h5>
</div>
    

   <div class="card-body card-body ">





        <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="txtNRoomSimple" class="tt_normsmedium">No de habitaciones Sencilla</label>
                            <input disabled validate min="0" value="<?=($regInfo[simple_room]!='') ? $regInfo[simple_room] : 0 ?>" id="txtNRoomSimple" type="number" class="form-control input-lg tt_normslight"  name="txtNRoomSimple" onclick="this.select();" >

                        </div>
                      <div class="form-group col-md-4">
                            <label for="txtNRoomDouble" class="tt_normsmedium">No de habitaciones Doble</label>
                            <input disabled validate min="0" value="<?=($regInfo[double_room]!='') ? $regInfo[double_room] : 0 ?>"   id="txtNRoomDouble" type="number" class="form-control input-lg tt_normslight"  name="txtNRoomDouble" onclick="this.select();" >

                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="txtNRoomTriple" class="tt_normsmedium">No de habitaciones Triple</label>
                            <input disabled validate min="0" value="<?=($regInfo[triple_room]!='') ? $regInfo[triple_room] : 0 ?>"   id="txtNRoomTriple" type="number" class="form-control input-lg tt_normslight"  name="txtNRoomTriple" onclick="this.select();" >

                        </div>

                      
                    </div>
  </div>
    
  </div>


<!--------------------------------------->

  <div class="card mt-3 wow animated fadeIn" style="width: 100%;" data-wow-delay="0.40s">  
  <div class="card-header card-header text-white bg-secondary">
  
<h5 class="m-0 p-0 tt_normsbold">1.2. Extensión (Pre o Post)</h5>
</div>
    

   <div class="card-body card-body ">
        <div class="form-row">

                        <div class="form-group col-md-4">
                             <label for="rdoExtension" class="tt_normsmedium">¿Tomarás extension (Pre o Post)?</label>
                              <div class="c-inputs-stacked p-0 pt-2">
                              <label class="c-input c-radio pr-3 ">
                              <input disabled autocomplete="off" <?=($regInfo["extension_detail"] == "yes") ? "checked" : "" ?> name="rdoExtension" type="radio" value="yes" >
                              <span class="c-indicator pl-2"></span> Si </label>
                            
                              <label class="c-input c-radio">
                              <input disabled autocomplete="off" <?=($regInfo["extension_detail"] == "no") ? "checked" : "" ?> name="rdoExtension" type="radio" value="no">
                              <span class="c-indicator pl-2"></span> No </label>
                            
                            </div>

                        </div>
                      <div class="form-group col-md-8">
                            
				<label class="" for="slExtension">¿Cuál?</label>

                           <select disabled  autocomplete="off" class="c-select form-control form-control-md" style="width:100%;" id="slExtension" name="slExtension" aria-describedby="helpMap" >
							                  <option selected value="">Seleccione una</option>
							                  <?php 
												  $sql = "SELECT
    `id`
    , `extension_name`
FROM
    `EXTENSIONS` 
where `id`<>0
    order by extension_name asc";
												
												$micon->query($sql);
												while($ext = $micon->fetchArray()){
											  ?>
							                  <option <?=($regInfo["extension"] == $ext["id"]) ? "selected" : "" ?> value="<?=$ext["id"]?>"><?=$ext["extension_name"]?></option>
							                   <? } ?>
							                </select>

                        </div>
                        
                        

                      
                    </div>
  </div>
    
  </div>


<!--------------------------------------->

  <div class="card mt-3 wow animated fadeIn" style="width: 100%;" data-wow-delay="0.50s">  
  <div class="card-header card-header text-white bg-secondary">
  
<h5 class="m-0 p-0 tt_normsbold">2. Indique su forma de pago</h5>
</div>
    

   <div class="card-body card-body ">
        

      

          
        
        <div class="form-row">

                      <div class="form-group col-md-5 ">   
                       <label  class="tt_normsmedium">2.1</label> 
                      <div class="c-inputs-stacked p-0 " style="    margin-top: 12px;">
                          <label class="c-input c-radio pr-2 ">
                              <input disabled <?=($regInfo["payment"] == "21") ? "checked" : "" ?>  class=""  autocomplete="off"  name="rdoPayment" type="radio" value="21" >
                              <span class="c-indicator pl-2"></span>  Pago de contado hasta el 31/08/2019 (Descuento del 9%)
                          </label>
                            
                             
                            
                      </div>

                      </div>

                      <div class="form-group col-md-7">
                            <label for="txt21" class="tt_normsmedium">$</label>  
                            <input   disabled id="txt21" type="number" class="form-control input-lg tt_normslight" value="<?=($regInfo["payment"] == "21") ? $regInfo["money"] : "" ?>"  name="txt21"  onclick="this.select();">

                      </div>
                        
                      
                      
          </div>

          <div class="form-row">

                      <div class="form-group col-md-5 ">   
                      <label  class="tt_normsmedium">2.2</label> 
                      <div class="c-inputs-stacked p-0 " >
                          <label class="c-input c-radio pr-3 pt-2">
                              <input disabled <?=($regInfo["payment"] == "22") ? "checked" : "" ?>  class=""  autocomplete="off"  name="rdoPayment" type="radio" value="22" >
                              <span class="c-indicator pl-2"></span>  Pago hasta el 20/12/2019 (Descuento del 5%) 
                          </label>
                            
                             
                            
                      </div>

                      </div>
                      
                      <div class="form-group col-md-2">
                            <label  for="txtQ22" class="tt_normsmedium">No Cuotas</label>  
                            <input  disabled id="txtQ22" type="number" class="form-control input-lg tt_normslight" value="<?=($regInfo["payment"] == "22") ? $regInfo["quotes"] : "" ?>"  name="txtQ22"  onclick="this.select();">

                      </div>

                      <div class="form-group col-md-5">
                            <label for="txtMoney22" class="tt_normsmedium">$</label>  
                            <input min="1" disabled id="txtMoney22" type="number" class="form-control input-lg tt_normslight" value="<?=($regInfo["payment"] == "22") ? $regInfo["money"] : "" ?>" name="txtMoney22"  onclick="this.select();">

                      </div>
                                                                    
          </div>

          <div class="form-row">

                      <div class="form-group col-md-5 ">   
                      <label  class="tt_normsmedium">2.3</label> 
                      <div class="c-inputs-stacked p-0 " >
                          <label class="c-input c-radio pr-3 pt-2">
                              <input disabled <?=($regInfo["payment"] == "23") ? "checked" : "" ?>  class=""  autocomplete="off"  name="rdoPayment" type="radio" value="23" >
                              <span class="c-indicator pl-2"></span>  Pago a cuotas (hasta el 30 abril 2021) 
                          </label>
                            
                             
                            
                      </div>

                      </div>
                      
                      <div class="form-group col-md-2">
                            <label for="txtQ23" class="tt_normsmedium">No Cuotas</label>  
                            <input min="1" disabled id="txtQ23" type="number" class="form-control input-lg tt_normslight" value="<?=($regInfo["payment"] == "23") ? $regInfo["quotes"] : "" ?>" name="txtQ23"  onclick="this.select();">

                      </div>

                      <div class="form-group col-md-5">
                            <label for="txtMoney23" class="tt_normsmedium">$</label>  
                            <input min="1" disabled  id="txtMoney23" type="number" class="form-control input-lg tt_normslight" value="<?=($regInfo["payment"] == "23") ? $regInfo["money"] : "" ?>" name="txtMoney23"  onclick="this.select();">

                      </div>
                                                                    
          </div>


           <div class="form-row">

                                            
                      <div class="form-group col-md-6">
                            <label  for="txtStartQuotingDate" class="tt_normsmedium">Fecha de inicio de cobro de la cuota </label>  
                            <input disabled required id="txtStartQuotingDate" type="date" class="form-control input-lg tt_normslight"  name="txtStartQuotingDate" value="<?=$regInfo["start_quotes_date"]?>" >

                      </div>

                      <div class="form-group col-md-6">
                            <label for="txtFinishQuotingDate" class="tt_normsmedium">Fecha de fin de cobro de la cuota</label>  
                            <input disabled id="txtFinishQuotingDate" type="date" class="form-control input-lg tt_normslight"  name="txtFinishQuotingDate" value="<?=$regInfo["end_quotes_date"]?>"  >

                      </div>
                                                                    
          </div>





  </div>
    
  </div>


 <div class="row my-3">

               <? if (!$_GET[a]=='upload'){ ?>                             
   <div class="col">
				<a target="_blank" href="php/pdf_report.php?id=<?=$id?>" data-wow-delay="0.60s" class="btn  btn-lg btn-block   btn-warning tt_normslight  wow  fadeIn" ><i class="fal fa-file-pdf"></i> Ver pdf </a>
			</div>

    <? } ?>
	<div class="col">		
			<a href="#" data-wow-delay="0.70s" id="btnSave" class="btn  btn-lg btn-block   btn-warning tt_normslight  wow  fadeIn" ><i class="fal fa-upload"></i> Subir formulario firmado </a>
	</div>
</div>


</form>

</div>
</div>
</div>


<div class="modal fade" id="msgModalSave" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="msgTitleSave" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header tt_normsbold">
        <h5 class="modal-title" id="msgTitleSave"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body tt_normslight text-center" id="msgBodySave">
         
      </div>
      <div class="modal-footer">
        
        <a href="list.php" class="btn btn-outline-warning text-warning" >Volver al listado</a>
        <button type="button" id="btnOkSave"  data-dismiss="modal" class="btn btn-warning " ></button>

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="msgModal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="msgTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header tt_normsbold">
        <h5 class="modal-title" id="msgTitle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body tt_normslight text-center" id="msgBody">
         
      </div>
      <div class="modal-footer">
        
        <a href="list.php" class="btn btn-outline-warning text-warning" >Volver al listado</a>
        <button type="button" id="btnOk"  data-dismiss="modal" class="btn btn-warning " ></button>

      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="msgModalConfirm" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="msgTitleCOnfirm" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header tt_normsbold">
        <h5 class="modal-title" id="msgTitleCOnfirm">Confirmación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body tt_normslight text-center" >
         
         <i class="d-block fa-8x fal fa-question-circle pb-2 text-secondary"></i>
         ¿Estás seguro que deseas guardar la información ingresada?. <br><br><b>Recuerda:</b><br> Una vez guardado no se pueden hacer cambios sobre éste.
      </div>
      <div class="modal-footer">
        
        <a  data-dismiss="modal" class="btn btn-outline-warning text-warning" >Cancelar </a>
        <button type="button" id="btnConfirmSave" data-dismiss="modal" class="btn btn-warning " > Guardar</button>

      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="msgModalFormError" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="msgTitleFormError" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header tt_normsbold">
        <h5 class="modal-title" id="msgTitleFormError">Oops!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body tt_normslight text-center" >
         
         <i class="d-block fa-8x fal fa-exclamation-triangle pb-2 text-warning"></i>
         Ocurrió un error al intentar guardar la información, esto se debe a que hace falta información por ingresar.
         <span id="errorResume"></span>
      </div>
      <div class="modal-footer">
        
        <!-- <a  data-dismiss="modal" class="btn btn-outline-warning text-warning" >Cancelar </a> -->
        <button type="button"  data-dismiss="modal" class="btn btn-warning " >Revisar</button>

      </div>
    </div>
  </div>
</div>

<script src="js/jquery-3.3.1.js"></script>  
<!-- <script src="js/jquery.mask.js"></script>   -->
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/messages_es.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/terpel-jquery.js"></script>

<script type="text/javascript">
	
var modalActionOnClose = false
function showMsg(title, msg,btnText,reload){
  	$("#msgTitle").html(title)
		$("#msgBody").html(msg)
		$("#btnOk").html(btnText)

		modalActionOnClose = reload
  		$('#msgModal').modal({
		    backdrop: 'static',
		    keyboard: false
		})
}


var modalActionSaveOnClose = false
function showMsgSave(title, msg,btnText,print){
    $("#msgTitleSave").html(title)
    $("#msgBodySave").html(msg)
    $("#btnOkSave").html(btnText)

    modalActionSaveOnClose = print
      $('#msgModalSave').modal({
        backdrop: 'static',
        keyboard: false
    })
}


  $('#msgModalSave').on('hidden.bs.modal', function (e) {
    if(modalActionSaveOnClose){
     location.href = "https://convenciondealiadosterpel.com/registro/php/pdf_report.php?id=" + <?=$id?>; 
    }
  })     

function showFormError(msg){
    
    $("#errorResume").html(msg)    

    //modalActionOnClose = reload
    $('#msgModalFormError').modal({
        backdrop: 'static',
        keyboard: false
    })
}


$(window).on("load",function() {


        $(".pageLoader").fadeOut("fast", function() {
           new WOW().init();
       });

   });
  $(document).ready(function() {


    
    //$('#txtMoney22').mask('#.##0,00', {reverse: true});
    //$('#txtMoney22').mask('000.000.000.000.000', {reverse: true});

	$('#msgModal').on('hidden.bs.modal', function (e) {
 		if(modalActionOnClose){
 			window.location.reload();
 		}
	})		 

  	$("input[name='rdoHasPassPort0']").click(function(){
  		var rdoHasPassPort0 = this.value;
  		if (rdoHasPassPort0=="no"){
  			$("#txtPassport0").attr("disabled",true).addClass("disabled")
  			$("#file-passPort0").attr("disabled",true).addClass("disabled")
  			$("#label-file-passPort0").attr("disabled",true).addClass("disabled")
  		}
  		else{
  			$("#txtPassport0").attr("disabled",false).removeClass("disabled")
  			$("#file-passPort0").attr("disabled",false).removeClass("disabled")
  			$("#label-file-passPort0").attr("disabled",false).removeClass("disabled")
  		}

  	})

  	$("input[name='rdoHasPassPort1']").click(function(){
  		var rdoHasPassPort1 = this.value;
  		if (rdoHasPassPort1=="N"){
  			$("#txtPassport1").attr("disabled",true).addClass("disabled")
  			$("#file-passPort1").attr("disabled",true).addClass("disabled")
  			$("#label-file-passPort1").attr("disabled",true).addClass("disabled")
  		}
  		else{
  			$("#txtPassport1").attr("disabled",false).removeClass("disabled")
  			$("#file-passPort1").attr("disabled",false).removeClass("disabled")
  			$("#label-file-passPort1").attr("disabled",false).removeClass("disabled")
  		}

  	})


  	$("input[name='rdoAgeRange1']").click(function(){
  		var rdoAgeRange1 = this.value;
  		if (rdoAgeRange1=="Adulto"){
  			$("#txtAge1").attr("disabled",true).addClass("disabled")
  			
  		}
  		else{
  			$("#txtAge1").attr("disabled",false).removeClass("disabled")

  		}

  	})


    $("input[name='rdoCompanion']").click(function(){
      var rdoCompanion = this.value;
      if (rdoCompanion=="0"){
        $("#txtCompanion1").attr({"disabled":true,"required":false }).addClass("disabled")      
        $("input[name='rdoHasPassPort1']").attr({"disabled":true,"required":false }).addClass("disabled")
        $("#txtPassport1").attr({"disabled":true,"required":false }).addClass("disabled")
        $("input[name='rdoAgeRange1']").attr({"disabled":true,"required":false }).addClass("disabled")
        $("#txtAge1").attr({"disabled":true,"required":false }).addClass("disabled")
        $("#label-file-passPort1").attr({"disabled":true,"required":false }).addClass("disabled")
        $("#file-passPort1").attr({"disabled":true,"required":false }).addClass("disabled")
        $("#view-file-1").attr({"disabled":true,"required":false }).addClass("disabled")

        $("#txtCompanion2").attr({"disabled":true,"required":false }).addClass("disabled")      
        $("input[name='rdoHasPassPort2']").attr({"disabled":true,"required":false }).addClass("disabled")
        $("#txtPassport2").attr({"disabled":true,"required":false }).addClass("disabled")
        $("input[name='rdoAgeRange2']").attr({"disabled":true,"required":false }).addClass("disabled")
        $("#txtAge2").attr({"disabled":true,"required":false }).addClass("disabled")
        $("#label-file-passPort2").attr({"disabled":true,"required":false }).addClass("disabled")
        $("#file-passPort2").attr({"disabled":true,"required":false }).addClass("disabled")


        $( "#liCompanion1" ).find( ".c-input" ).removeClass( "state-error" );
        $( "#liCompanion2" ).find( ".c-input" ).removeClass( "state-error" );

      //  $("#view-file-2").attr({"disabled":true,"required":false }).addClass("disabled")
        

        
      }
      if (rdoCompanion=="1" || rdoCompanion=="2"){   
          $("#txtCompanion1").attr({"disabled":false,"required":true }).removeClass("disabled")      
          $("input[name='rdoHasPassPort1']").attr({"disabled":false,"required":true }).removeClass("disabled")
          $("#txtPassport1").attr({"disabled":false,"required":true }).removeClass("disabled")
          $("input[name='rdoAgeRange1']").attr({"disabled":false,"required":true }).removeClass("disabled")
          $("#txtAge1").attr({"disabled":false,"required":true }).removeClass("disabled")
          $("#label-file-passPort1").attr({"disabled":false,"required":true }).removeClass("disabled")
          $("#file-passPort1").attr({"disabled":false,"required":true }).removeClass("disabled")
         
          if (  $("#view-file-1").attr("href") != "#" ){
              $("#view-file-1").attr({"disabled":false,"required":true }).removeClass("disabled")   
          }
          if ( $("input[name=rdoAgeRange1]:checked").val() == "Adulto" ){
              $("#txtAge1").attr({"disabled":true,"required":false }).addClass("disabled")
          }




          if(rdoCompanion=="1"){
               $("#txtCompanion2").attr({"disabled":true,"required":false }).addClass("disabled")      
              $("input[name='rdoHasPassPort2']").attr({"disabled":true,"required":false }).addClass("disabled")
              $("#txtPassport2").attr({"disabled":true,"required":false }).addClass("disabled")
              $("input[name='rdoAgeRange2']").attr({"disabled":true,"required":false }).addClass("disabled")
              $("#txtAge2").attr({"disabled":true,"required":false }).addClass("disabled")
              $("#label-file-passPort2").attr({"disabled":true,"required":false }).addClass("disabled")
              $("#file-passPort2").attr({"disabled":true,"required":false }).addClass("disabled")
              
              $( "#liCompanion2" ).find( ".c-input" ).removeClass( "state-error" );
          }
          

      }
      if (rdoCompanion=="2"){
           $("#txtCompanion2").attr({"disabled":false,"required":true }).removeClass("disabled")      
          $("input[name='rdoHasPassPort2']").attr({"disabled":false,"required":true }).removeClass("disabled")
          $("#txtPassport2").attr({"disabled":false,"required":true }).removeClass("disabled")
          $("input[name='rdoAgeRange2']").attr({"disabled":false,"required":true }).removeClass("disabled")
          $("#txtAge2").attr({"disabled":false,"required":true }).removeClass("disabled")
          $("#label-file-passPort2").attr({"disabled":false,"required":true }).removeClass("disabled")
          $("#file-passPort2").attr({"disabled":false,"required":true }).removeClass("disabled")
          //$("#view-file-2").attr({"disabled":false,"required":true }).removeClass("disabled") 
          if (  $("#view-file-2").attr("href") != "#" ){
              $("#view-file-2").attr({"disabled":false,"required":true }).removeClass("disabled")   
          }
          if ( $("input[name=rdoAgeRange2]:checked").val() == "Adulto" ){
              $("#txtAge2").attr({"disabled":true,"required":false }).addClass("disabled")
          }
      }


    })


  	$("input[name='rdoPayment']").click(function(){
  		var rdoPayment = this.value;
  		if (rdoPayment=="21"){
  			$("#txt21").attr({"disabled":false,"required":true }).removeClass("disabled")

  			$("#txtStartQuotingDate").attr("disabled",true).addClass("disabled")
  			$("#txtFinishQuotingDate").attr("disabled",true).addClass("disabled")

  			$("#txtQ22").attr("disabled",true).addClass("disabled")
  			$("#txtMoney22").attr("disabled",true).addClass("disabled")

  			$("#txtQ23").attr("disabled",true).addClass("disabled")
  			$("#txtMoney23").attr("disabled",true).addClass("disabled")
  			
  			

  			
  			
  			
  		}
  		else{


  			$("#txtStartQuotingDate").attr("disabled",false).removeClass("disabled")
  			$("#txtFinishQuotingDate").attr("disabled",false).removeClass("disabled")

  			if (rdoPayment=="22"){ 
  				$("#txtQ22").attr({"disabled":false,"required":true }).removeClass("disabled")
	  			$("#txtMoney22").attr({"disabled":false,"required":true }).removeClass("disabled")

  				
  				$("#txt21").attr("disabled",true).addClass("disabled")

  				$("#txtQ23").attr("disabled",true).addClass("disabled")
  				$("#txtMoney23").attr("disabled",true).addClass("disabled")
  			}
  			else if(rdoPayment=="23"){

  				$("#txtQ23").attr({"disabled":false,"required":true }).removeClass("disabled")
	  			$("#txtMoney23").attr({"disabled":false,"required":true }).removeClass("disabled")

  				$("#txtQ22").attr("disabled",true).addClass("disabled")
	  			$("#txtMoney22").attr("disabled",true).addClass("disabled")

	  			$("#txt21").attr("disabled",true).addClass("disabled")
	  			
  			}

  		}

  	})


  	$("input[name='rdoHasPassPort2']").click(function(){
  		var rdoHasPassPort2 = this.value;
  		if (rdoHasPassPort2=="N"){
  			$("#txtPassport2").attr("disabled",true).addClass("disabled")
  			$("#file-passPort2").attr("disabled",true).addClass("disabled")
  			$("#label-file-passPort2").attr("disabled",true).addClass("disabled")
  		}
  		else{
  			$("#txtPassport2").attr("disabled",false).removeClass("disabled")
  			$("#file-passPort2").attr("disabled",false).removeClass("disabled")
  			$("#label-file-passPort2").attr("disabled",false).removeClass("disabled")
  		}

  	})


  	$("input[name='rdoAgeRange2']").click(function(){
  		var rdoAgeRange2 = this.value;
  		if (rdoAgeRange2=="Adulto"){
  			$("#txtAge2").attr("disabled",true).addClass("disabled")
  			
  		}
  		else{
  			$("#txtAge2").attr("disabled",false).removeClass("disabled")

  		}

  	})


		$("input[name='rdoExtension']").click(function(){
  		var rdoExtension = this.value;
  		if (rdoExtension=="no"){
  			$("#slExtension").attr({"disabled":true,"required":false }).addClass("disabled")
  			
  		}
  		else{
  			$("#slExtension").attr({"disabled":false,"required":true }).removeClass("disabled")

  		}

  	})

	var errorClass = 'invalid';
				var errorElement = 'div';
						// Initialize form validation on the registration form.
			  // It has the name attribute "registration"
			  $("form[name='frmRegistration']").validate({
			    // Specify validation rules
			    errorClass: errorClass,
				errorElement: errorElement,
				onkeyup: true, //turn off auto validate whilst typing
        rules: {
            txtNRoomSimple: {
                min:  {
                 param: 1,
                 depends: function(element) {
                     return (
                        ($('#txtNRoomSimple').val() == '' || $('#txtNRoomSimple').val() == 0) && 
                        ($('#txtNRoomDouble').val() == '' || $('#txtNRoomDouble').val() == 0) && 
                        ($('#txtNRoomTriple').val() == '' || $('#txtNRoomTriple').val() == 0)
                     );
                  }
                },
                 required:  {
                   depends: function(element) {
                       return (
                          ($('#txtNRoomSimple').val() == '' || $('#txtNRoomSimple').val() == 0) && 
                          ($('#txtNRoomDouble').val() == '' || $('#txtNRoomDouble').val() == 0) && 
                          ($('#txtNRoomTriple').val() == '' || $('#txtNRoomTriple').val() == 0)
                       );
                    }
                }
            },
            txtNRoomDouble: {
                min:  {
                 param: 1,
                 depends: function(element) {
                     return (
                        ($('#txtNRoomSimple').val() == '' || $('#txtNRoomSimple').val() == 0) && 
                        ($('#txtNRoomDouble').val() == '' || $('#txtNRoomDouble').val() == 0) && 
                        ($('#txtNRoomTriple').val() == '' || $('#txtNRoomTriple').val() == 0)
                     );
                  }
                },
                 required:  {
                   depends: function(element) {
                       return (
                          ($('#txtNRoomSimple').val() == '' || $('#txtNRoomSimple').val() == 0) && 
                          ($('#txtNRoomDouble').val() == '' || $('#txtNRoomDouble').val() == 0) && 
                          ($('#txtNRoomTriple').val() == '' || $('#txtNRoomTriple').val() == 0)
                       );
                    }
                }
            },
            txtNRoomTriple: {
                min:  {
                 param: 1,
                 depends: function(element) {
                     return (
                        ($('#txtNRoomSimple').val() == '' || $('#txtNRoomSimple').val() == 0) && 
                        ($('#txtNRoomDouble').val() == '' || $('#txtNRoomDouble').val() == 0) && 
                        ($('#txtNRoomTriple').val() == '' || $('#txtNRoomTriple').val() == 0)
                     );
                  }
                },
                 required:  {
                   depends: function(element) {
                       return (
                          ($('#txtNRoomSimple').val() == '' || $('#txtNRoomSimple').val() == 0) && 
                          ($('#txtNRoomDouble').val() == '' || $('#txtNRoomDouble').val() == 0) && 
                          ($('#txtNRoomTriple').val() == '' || $('#txtNRoomTriple').val() == 0)
                       );
                    }
                }
            },
        },

				highlight: function (element) {
					/*console.log(element.type);
					console.log(element.name);*/

					if (element.type=="radio"){
							$(element).parent().removeClass('state-success').addClass("state-error");
							$(element).parent().removeClass('valid');
					}
					else{

						$(element).removeClass('state-success').addClass("state-error");
						$(element).removeClass('valid');
					}
				},
				unhighlight: function (element) {

					if (element.type=="radio"){
						$(element).parent().removeClass("state-error").addClass('state-success');
						$(element).addClass('valid');
					}
					else{
						$(element).removeClass("state-error").addClass('state-success');
						$(element).addClass('valid');
					}
				},
			   
			    // Specify validation error messages
			    
			    // Make sure the form is submitted to the destination defined
			    // in the "action" attribute of the form when valid
			    submitHandler: function(form) {
			      //form.submit();
			    },
			    errorPlacement: function (error, element) {
					//error.insertAfter(element);
				}
			  });


$('#btnSaveDraft').click( function() {
	console.log("clicked");
	

	var formElement = document.getElementById("frmRegistration");
	formData = new FormData(formElement);
	console.log(formData);

	var jqxhr = $.ajax({
			method: "POST",
			url: 'php/saveDraft.php' ,

			data: formData,
			dataType: "json",
			processData: false,
			contentType: false,
		//	contentType: 'multipart/form-data',
			beforeSend: function( xhr ) {
				$("#btnSaveDraft").html("<i class='fal  fa-circle-notch fa-spin'></i> Guardando borrador").addClass("disabled")
				$("#btnSave").addClass("disabled")
			 }
		})	
	.done(function(data) {
		
		if (data.result){
			
            icon = '<i class="d-block fa-8x fa-check-circle fal pb-2 text-success"></i>';
            msg  = 'La información se guardó correctamente en borrador';
            btn  = "Perfecto, recargar"

            files = ""
            if(!data.file0){
                files = '<br>* Pasaporte del Usuario'
            }
            if (!data.file1){
                files += '<br>* Pasaporte del acompañante No 1'
            }
            if (!data.file2) {
                files += '<br>* Pasaporte del acompañante No 2'
            }

            if (files!=""){
                icon = '<i class="d-block fa-8x fa-exclamation-triangle fal pb-2 text-warning"></i>';
                msg += "<br><br>Pero los siguientes archivos no se pudieron cargar:" + files
                btn = "Entendido"
            }

      			showMsg("Borrador guardado",icon + msg,btn,true);

			
		}else{

			//if (data.file0==1){
				
			//}
      if (data.validSession){
          showMsg("Error procesando solicitud",'<i class="d-block fa-8x fa-times-circle fal pb-2 text-danger"></i> No se pudo guardar la información ingresada',"Entendido",false);
      }
      else{
          showMsg("Sesión expirada",'<i class="d-block fa-8x fa-times-circle fal pb-2 text-danger"></i> No se pudo guardar la información ingresada. Tu sesión se ha vencido',"Entendido",true);
      }
			
			
		}
		
	})
	.fail(function(jqXHR, textStatus) {
		var msgTitle = "Error procesando solicitud";
		if(textStatus== 'timeout'){
		     //console.log("La conexión está tomando más tiempo de lo habitual para calificar esta idea. <br><br>Por favor inténtelo nuevamente.");
			//alert(" La conexión está tomando más tiempo de lo habitual para radicar esta factura . \n\nPor favor inténtelo nuevamente.")				
			showMsg(msgTitle,'<i class="d-block fa-8x fa-wifi-slash fal pb-2 text-danger"></i>  La conexión está tomando más tiempo de lo habitual para procesar su solicitud, Por favor inténtelo nuevamente.',"Entendido",false);
		}
		else{
			//alert("2 Ocurrió un error al intentar radicar esta factura.\n\nPor favor inténtelo nuevamente.")
			showMsg( msgTitle,'<i class="d-block fa-8x fa-wifi-slash fal pb-2 text-danger"></i> Ocurrió un error al procesar su solicitud',"Entendido",false);
			
		}

		//$("#btnSendQuotation").button('reset');
	//					alert('Ocurrió un error al actualizar tus datos');
	})
	.always(function() {	

		$("#btnSaveDraft").html('<i class="fal fa-pencil-ruler"></i> Guardar borrador').removeClass("disabled")
		$("#btnSave").removeClass("disabled")
	});

});

$('#frmRegistration').submit( function(e) {    
   e.preventDefault();
   console.log("frmRegistration submit");
   var formElement = document.getElementById("frmRegistration");
  formData = new FormData(formElement);
  console.log(formData);

  var jqxhr = $.ajax({
      method: "POST",
      url: 'php/saveFinal.php' ,

      data: formData,
      dataType: "json",
      processData: false,
      contentType: false,
    //  contentType: 'multipart/form-data',
      beforeSend: function( xhr ) {
        $("#btnSave").html("<i class='fal  fa-circle-notch fa-spin'></i> Guardando datos").addClass("disabled")
        $("#btnSaveDraft").addClass("disabled")
       }
    })  
  .done(function(data) {
    
    if (data.result){
      
            icon = '<i class="d-block fa-8x fa-check-circle fal pb-2 text-success"></i>';
            msg  = 'La información se guardó correctamente ';
            btn  = "Perfecto, ver e imprimir para firmar"

            files = ""
            if(!data.file0){
                files = '<br>* Pasaporte del Usuario'
            }
            if (!data.file1){
                files += '<br>* Pasaporte del acompañante No 1'
            }
            if (!data.file2) {
                files += '<br>* Pasaporte del acompañante No 2'
            }

            title = 'Datos guardados'
            if (files!=""){
                icon = '<i class="d-block fa-8x fa-exclamation-triangle fal pb-2 text-warning"></i>';
                title = 'Datos guardado en borrador'
                msg += "<br><br>Pero el registro quedó en borrador porque los siguientes archivos no se pudieron subir:" + files
                btn = "Entendido"
            }

            showMsgSave(title,icon + msg,btn,true);
            /* showMsgSave
msgTitleSave
msgBodySave
btnOkSave
msgModalSave */

      
    }else{

      //if (data.file0==1){
        
      //}
      if (data.validSession){
          showMsgSave("Error procesando solicitud",'<i class="d-block fa-8x fa-times-circle fal pb-2 text-danger"></i> No se pudo guardar la información ingresada',"Entendido",false);
      }
      else{
          showMsgSave("Sesión expirada",'<i class="d-block fa-8x fa-times-circle fal pb-2 text-danger"></i> No se pudo guardar la información ingresada. Tu sesión se ha vencido',"Entendido",true);
      }
      
      
    }
    
  })
  .fail(function(jqXHR, textStatus) {
    var msgTitle = "Error procesando solicitud";
    if(textStatus== 'timeout'){
         //console.log("La conexión está tomando más tiempo de lo habitual para calificar esta idea. <br><br>Por favor inténtelo nuevamente.");
      //alert(" La conexión está tomando más tiempo de lo habitual para radicar esta factura . \n\nPor favor inténtelo nuevamente.")        
      showMsgSave(msgTitle,'<i class="d-block fa-8x fa-wifi-slash fal pb-2 text-danger"></i>  La conexión está tomando más tiempo de lo habitual para procesar su solicitud, Por favor inténtelo nuevamente.',"Entendido",false);
    }
    else{
      //alert("2 Ocurrió un error al intentar radicar esta factura.\n\nPor favor inténtelo nuevamente.")
      showMsgSave( msgTitle,'<i class="d-block fa-8x fa-wifi-slash fal pb-2 text-danger"></i> Ocurrió un error al procesar su solicitud',"Entendido",false);
      
    }

    //$("#btnSendQuotation").button('reset');
  //          alert('Ocurrió un error al actualizar tus datos');
  })
  .always(function() {  

    $("#btnSave").html('<i class="fal fal fa-save"></i> Guardar datos').removeClass("disabled")
    $("#btnSaveDraft").removeClass("disabled")
  });
})

$('#btnSave').click( function() {   
   
  console.log("btnSave click");
   
   var files = '';
    var hasError = false
    if ($("input[name=rdoHasPassPort0]:checked").val() == 'yes' && $("#view-file-0").attr("href")=="#" && $("#file-passPort0").val()=='' ){
      $("#label-file-passPort0").removeClass("btn-secondary").addClass("btn-danger")
      files = "<br>*Pasaporte del aliado"
      hasError = true
    }
    else{
      $("#label-file-passPort0").removeClass("btn-danger").addClass("btn-secondary")
    }

    if ($("input[name=rdoHasPassPort1]:checked").val() == 'S' && $("#view-file-1").attr("href")=="#" && $("#file-passPort1").val()=='' ){
      $("#label-file-passPort1").removeClass("btn-secondary").addClass("btn-danger")
      files += "<br>*Pasaporte del acompañante #1"
      hasError = true
    }
    else{
      $("#label-file-passPort1").removeClass("btn-danger").addClass("btn-secondary")
    }

    if ($("input[name=rdoHasPassPort2]:checked").val() == 'S' && $("#view-file-2").attr("href")=="#" && $("#file-passPort2").val()=='' ){
      $("#label-file-passPort2").removeClass("btn-secondary").addClass("btn-danger")
      files += "<br>*Pasaporte del acompañante #2"
      hasError = true
    }
    else{
      $("#label-file-passPort2").removeClass("btn-danger").addClass("btn-secondary")
    }



    if ($('#frmRegistration').valid()){
        if (hasError){
            showFormError('<br><br><b>Revisar también</b>' + files)
        }
        else{
            $('#msgModalConfirm').modal()
        }
    }
    else{
        //showFormError('<br><br><b>Revisar también 2</b>' + files)
        if (hasError){
            showFormError('<br><br><b>Revisar también</b>' + files)
        }
        else{
            showFormError('')
        }
    }

})


$('#btnConfirmSave').click( function() {
        $("#frmRegistration").submit();

});				






    



  });

</script>


</body>
</html>