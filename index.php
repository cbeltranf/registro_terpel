<?
session_start();
if($_SESSION['auth_register'] == 'true'){
    header("Location: dashboard.php");
    exit();
}
//print_r($_SERVER);
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <title>Registro · Terpel</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="icon" href="images/icons/favicon.ico" type="image/x-icon">
  <meta property="og:type" content="website"/>
  <meta property="og:image" content="https://convenciondealiadosterpel.com/prereg/icons/apple-icon-180x180.png"/>
  <meta property="og:url" content="https://convenciondealiadosterpel.com/prereg"/>
  <meta property="og:title" content="Registro · Terpel"/>
  
  <?php include_once("analyticstracking.php") ?>
 <link rel="apple-touch-icon" sizes="57x57" href="icons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="icons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="icons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="icons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="icons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="icons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="icons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="icons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="icons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="icons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/icons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="images/icons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/icons/favicon-16x16.png">
<link rel="manifest" href="images/icons/manifest.json">
<meta name="msapplication-TileColor" content="#C30B13">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#C30B13">



  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/all.min.css">
  <link rel="stylesheet" href="css/terpel-fonts.css">
  <link rel="stylesheet" href="css/inmov.css">
  <link rel="stylesheet" href="css/animate.css">


<style type="text/css">
   @media (min-width: 768px) and (max-width: 5000px) { 
    #main_content {    
      background: url(images/bg-wide-<?=rand(1, 4);?>.jpg) no-repeat center  left fixed;   
      -webkit-background-size: cover;
      -moz-background-size: cover;
      background-size: cover;
      -o-background-size: cover;
     } 
  }
</style>
</head>
<body>

  

 <div class="pageLoader"></div>
<div class="container-fluid" id="main_content">
  <div class="row">
  <div class="col-xl-8 col-lg-7 col-md-5  pl-0">
    
    
    <nav class="navbar navbar-light pl-0 ">

    <img src="images/logo-terpel.svg"  class="d-inline-block align-top" alt="" style="width: 30%; max-width: 150px; ">

  </nav>


  </div>


  <div class="col-xl-4 col-lg-5 col-md-7 red-column  px-0 p-sm-2 d-flex  justify-content-center" id="red-column">
  
  


    <div class="container-fluid text-center pt-2 pb-2" >

          <div class="row ">
               <div class="col">
                <img class="w-75 wow fadeIn " style="visibility: hidden;" data-wow-duration="0.6s" data-wow-delay="0.3s" src="images/mas-por-descubrir.svg">
              </div>
          </div>

          
          <form id="frmLogin" method="post" action="php/login.php" name="frmLogin" class="p-xl-5 p-lg-3 p-md-3 px-0 pt-3 pb-3 wow fadeIn needs-validation login" novalidate  style="visibility: hidden;" data-wow-duration="1s" data-wow-delay="0.6s">
                  <div class="form-group">
                    <label for="email" class="text-white tt_normsmedium">Iniciar Sesión<br>
                    </label>
                    <input required autocomplete="false" type="email" class="form-control form-control-lg wow <?=($_GET[e]==2)?"is-invalid" : "" ?>" id="email" name="email" aria-describedby="email" placeholder="Email" value="">
                    <div class="m-0 p-1 bg-danger text-warning invalid-feedback text-left  rounded tt_normslight wow fadeIn">
                   <i class="fal fa-exclamation-triangle"></i> Debes ingresar un Email válido.
                  </div>
                  <div class="form-group" style="margin-top: 5px;">
                    <input required autocomplete="false" type="password" class="form-control form-control-lg wow <?=($_GET[e]==2)?"is-invalid" : "" ?>" id="password" name="password" aria-describedby="password" placeholder="Password" value="">
                    <div class="m-0 p-1 bg-danger text-warning invalid-feedback text-left  rounded tt_normslight wow fadeIn">
                   <i class="fal fa-exclamation-triangle"></i> Debes ingresar tu password.
                  </div>

                    <!-- <small id="txtNitHelp" class="form-text text-white  tt_normslight">
                    
                    ¿Tienes algún inconveniente? Escríbenos a <a href="mailto:dev@inmov.com" class="text-warning">dev@inmov.com</a> 
                    ¿Al ingresar tu NIT aparece como no válido? Escríbenos a <a href="mailto:convenciondealiados@terpel.com" class="text-warning">convenciondealiados@terpel.com  </a>
                    
                    
                                      </small> -->
                  </div>
                  
                  
                  <button type="submit" class="btn btn-block btn-lg btn-warning tt_normslight wow  fadeInDown"  style="visibility: hidden;" data-wow-duration="1s" data-wow-delay="0.9s"  id="btnLogin"><i class="fal fa-sign-in"></i> Ingresar</button>


                  


                </form>
          
          <?php
          $msgTitle = "";
    if ($_GET[e]){
      
      ?>


  
          <?php
     switch ($_GET[e]){
      case 1:
           $msgTitle = 'Email no encontrado';
           $msgBody  = 'No hemos encontrado en nuestros registros el email ' . $_SESSION[email] .', por favor escríbenos a: <a href="mailto:convenciondealiados@terpel.com" class="text-danger">convenciondealiados@terpel.com </a>. ';
   
    

    break;
    case 2:
           $msgTitle = 'Email vacío';
           $msgBody  = ' Ingresar al panel debes ingresar un email.';
   
    break;
    case 3:
           $msgTitle = 'Sesión caducada';
           $msgBody  = 'Queremos saber quien eres, antes de continuar debes identificarte.';

    break;

   }
    //$msgBody .=  '<br><br> ¿Algo diferente falló? escríbenos a <a href="mailto:dev@inmov.com" class="text-danger">dev@inmov.com</a>';
   ?>
  
   <?php
    }
   ?> 

          <div class ="row">
            <div class="col">
                <img class ="w-25 pt-3 wow bounceIn"  style="visibility: hidden;" data-wow-duration="1s" data-wow-delay="1.3s" src="images/convencion-aliados-19.svg">  
                
                <br>
                     <img class =" pt-5 w-25 wow bounceIn"  style="visibility: hidden;"  data-wow-duration="1s" data-wow-delay="1.3s" src="images/terpel.svg">  

             </div>
          </div>
    
        
  </div>
</div>
</div>

<div class="modal fade" id="msgModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header tt_normsbold">
        <h5 class="modal-title" id="exampleModalLabel"><?=$msgTitle?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body tt_normslight">
         <?=$msgBody?>
      </div>
      <div class="modal-footer">
        
        <button type="button" id="btnUnderStood" class="btn btn-danger" >Entendido</button>
      </div>
    </div>
  </div>
</div>

<script src="js/jquery-3.3.1.js"></script>  
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/messages_es.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/terpel-jquery.js"></script>
<script type="text/javascript">
 $(window).on("load",function() {
     // alert("window load")
      $(".pageLoader").fadeOut("fast", function() {
        //   alert("pageLoader fadeOut")
           new WOW().init();
       });
        <? if ($msgTitle != ""){ ?>
            $('#msgModal').modal()
        <? } ?>


        if ( $(window).height() < $(window).width() ){

            if (  $(window).height() >= $(".red-column").outerHeight()  ){
                $(".red-column").addClass("align-items-center");
                $(".red-column").css("height", $(window).height() )
            }
            else{
                 $(".red-column").removeClass("align-items-center");
            }
            



        }
        else{
            $(".red-column").css("height", $(window).height() )
            $(".red-column").addClass("align-items-center");
        }


        
       

        setTimeout(function(){ 
                $('#txtNit').addClass('shake'); 
        }, 3000);




    });
  $(document).ready(function() {

    
   // alert("document ready")
   

    $(window).on("resize",function() {
       //$(".red-column").css("height", $(window).height() )

        if ( $(window).height() < $(window).width() ){
            if (  $(window).height() >= $(".red-column").outerHeight()  ){
                $(".red-column").addClass("align-items-center");
                $(".red-column").css("height", $(window).height() )
            }
            else{
                 $(".red-column").removeClass("align-items-center");
            }
        }
        else{
            $(".red-column").css("height", $(window).height() )
            $(".red-column").addClass("align-items-center");
        }
        console.log("resize");
        //$(".pageLoader").fadeOut("slow");
    });

  });

</script>


</body>
</html>