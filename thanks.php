<?php 
session_start();
require( 'include/security.php' );
require( 'include/utils.php' );
require( 'include/mysql_class.php' );


//GET SYSTEM CONFIG CLOSE DATE
$sql = "SELECT value FROM `CONFIG` WHERE `ITEM` = 'close_date' ";
$micon->query( $sql );
$close_date = $micon->fetchArray()[ value ];
$now = date( "Y-m-d H:i:s" );
//echo $close_date;
//$close_date
//



//GET DATA USER
$sql = "SELECT * FROM `USER` WHERE `id` = '$_SESSION[nit]' ";
$micon->query( $sql );
$dataUserDetail = $micon->fetchArray();
//

//GET FIRSTANAME USER FOR WELCOME
$first_name = mb_convert_case( trim( $dataUserDetail[ "first_name" ] ), MB_CASE_TITLE, "UTF-8" ) ;


?><!DOCTYPE html>
	<html lang="es">
		<head>
			<title>Gracias</title>
			<meta charset="utf-8">	
			
  <?php include_once("analyticstracking.php") ?>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			
			<link rel="icon" href="images/icons/favicon.ico" type="image/x-icon">
			<meta property="og:type" content="website"/>
			<meta property="og:image" content="https://convenciondealiadosterpel.com/prereg/icons/apple-icon-180x180.png"/>
			<meta property="og:url" content="https://convenciondealiadosterpel.com/prereg"/>
			<meta property="og:title" content="Pre-registro · Terpel"/>
			
			
	<link rel="apple-touch-icon" sizes="57x57" href="icons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="icons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="icons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="icons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="icons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="icons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="icons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="icons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="icons/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="icons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="images/icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="images/icons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="images/icons/favicon-16x16.png">
	<link rel="manifest" href="images/icons/manifest.json">
	<meta name="msapplication-TileColor" content="#C30B13">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#C30B13">



	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/all.min.css">
	<link rel="stylesheet" href="css/terpel-fonts.css">
	<link rel="stylesheet" href="css/inmov.css">
	<link rel="stylesheet" href="css/animate.css">
	<script src="js/jquery-3.3.1.js">	</script>
	

	<style type="text/css">
		@media (min-width: 768px) and (max-width: 5000px) {
			#main_content {
				background: url(images/bg-wide-<?=rand(1, 4);
				?>.jpg) no-repeat center left fixed;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
				-o-background-size: cover;
			}
		}

	</style>
</head>

<body>
	<div class="pageLoader"></div>
	<div class="container-fluid" id="main_content">
		<div class="row">
			<div class="col-xl-8 col-lg-7 col-md-5  pl-0">
    
    
			    <nav class="navbar navbar-light pl-0 ">

			    <img src="images/logo-terpel.svg"  class="d-inline-block align-top" alt="" style="width: 30%;    max-width: 150px; ">

			  </nav>


			  </div>
			<div class="col-xl-4 col-lg-5 col-md-7 red-column  pl-0 pr-0 p-sm-2 d-flex  justify-content-center" id="red-column">

				<div class="container-fluid text-center pt-2 pb-2">

		
<div class="red-column-content">
<img class=" wow fadeIn pb-2" style="visibility: hidden;" style="    margin-top: -32px !importat;  " data-wow-duration="0.6s" data-wow-delay="0.3s" src="images/gracias.svg">




						<a href="../index.php" style="visibility: hidden; font-weight: bold;  margin-right: 10px;" class=" text-warning tt_normslight  wow  fadeInDown"><i class="fal fa-home"></i> Ir al Micrositio </a>
						<a href="php/logout.php" style="visibility: hidden;margin-right:10px; font-weight: bold;" class=" text-warning tt_normslight  wow  fadeInDown"><i class="fal fa-sign-out"></i> Volver al registro </a>  
						<BR>
						<?

?>





							
					</div>


			</div>
	
					</div>
					</div>
				




			

	<div class="modal fade" id="modalTerms" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">

<div class="modal-header">

<h4 class="modal-title" id="modalLabelLarge">Formato Autorización de uso de Imagen y de Datos Personales</h4>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
</div>

<div class="modal-body tt_normslight" style="    font-size: 16px;">
<p>En cumplimiento de las disposiciones de la Ley 1581 de 2012 y su Decreto reglamentario 1377 de 2013, que desarrollan el derecho de
habeas data, solicitamos su autorización para que Organización Terpel S.A. (en adelante Terpel) pueda recopilar, almacenar, archivar,
copiar, analizar, usar y consultar los datos e imágenes obtenidos con las siguientes nalidades, todos relacionados con las actividades
de Terpel y el ejercicio de su objeto social: a) informes institucionales internos y externos, presentaciones de voceros (representantes
legales, apoderados, presidente, vicepresidentes y empleados de Terpel) frente a diferentes órganos societarios y corporativos o ante
autoridades, campañas externas de mercadeo, campañas de comunicaciones internas, publicación de documentos institucionales
sobre información de la empresa; b) El envío de correspondencia, correos electrónicos o contacto telefónico con sus clientes,
proveedores y usuarios de sus distintos programas en desarrollo de actividades publicitarias, promocionales, de mercadeo
(principalmente para planes de delidad y relacionales) de ejecución de ventas, estudios de mercado enfocados a su actividad de
distribución de combustibles líquidos, lubricantes o GNV o prestación de servicios complementarios; c) Compartirla con terceros
aliados, proveedores y sociedades del mismo grupo empresarial ubicadas dentro o fuera del país, en particular para la realización de
actividades de conocimiento al cliente, relacionamiento comercial o publicitario y gestión de ventas. d) Transferencia y transmisión de
datos a terceros con quienes realice alianzas relacionadas con su objeto social, contrate estudios o les encargue el tratamiento de
datos. e) Mantenimiento por sí mismo o a través de un tercero, de las bases de datos; f) Para su uso en redes sociales, así como para la
elaboración de los informes de gestión de Terpel y su publicación en elementos comerciales o de mercadeo físicos o electrónicos
elaborados por Terpel o sus proveedores.
</p>
<p>
Los derechos que le asisten conforme a la ley al Titular de los datos son los siguientes: a) Conocer, actualizar y recticar sus datos
personales frente a los Responsables del Tratamiento o Encargados del Tratamiento. Este derecho se podrá ejercer, entre otros frente
a datos parciales, inexactos, incompletos, fraccionados, que induzcan a error, o aquellos cuyo Tratamiento esté expresamente
prohibido o no haya sido autorizado; b) Solicitar prueba de la autorización otorgada al Responsable del Tratamiento salvo cuando
expresamente se exceptúe como requisito para el Tratamiento de conformidad con lo previsto en el artículo 10 de la Ley 1581 de 2012;
c) Ser informado por el Responsable del Tratamiento o el Encargado del Tratamiento, previa solicitud, respecto del uso que le ha dado
a sus datos personales; d) Presentar ante la Superintendencia de Industria y Comercio quejas por infracciones a lo dispuesto en la
presente ley y las demás normas que la modiquen, adicionen o complementen; e) Revocar la autorización y/o solicitar la supresión
del dato cuando en el Tratamiento no se respeten los principios, derechos y garantías constitucionales y legales. La revocatoria y/o
supresión procederá cuando la Superintendencia de Industria y Comercio haya determinado que en el Tratamiento el Responsable o
Encargado han incurrido en conductas contrarias a esta ley y a la Constitución; f) Acceder en forma gratuita a sus datos personales
que hayan sido objeto de Tratamiento al menos una vez cada mes calendario y cada vez que existan modicaciones sustanciales de las
políticas de tratamiento de información que motiven nuevas consultas. g) Presentar consultas, peticiones y quejas en torno al manejo
dado por la entidad a su información personal y en general al ejercicio del Hábeas Data. h) Para efectos de registro, participación y
actividades de la Convención Terpel 2017 y posteriores invitaciones a eventos que se realicen a través de correspondencia, correos
electrónicos o contacto telefónico.</p>
<p>
Cuando se recolecten datos personales sensibles, o de niños, niñas o adolescentes, el Titular de los datos no estará obligado a
responder las preguntas que versen sobre los mismos ni a autorizar su tratamiento.
</p>
<p>
La vigencia de la base datos será la del periodo de tiempo en que se mantengan las nalidades del tratamiento en cada base de datos
o aquel requerido de acuerdo a las normas contractuales, contables, comerciales, tributarias, o cualquiera aplicable según la materia,
con un plazo máximo de cincuenta años, y prorrogables por períodos iguales según las necesidades de la entidad. Le informamos que
puede consultar la Política de tratamiento de la Información de Terpel en la página www.terpel.com, que contiene los lineamientos,
directrices, y procedimientos sobre el tratamiento de la información de terceros por parte de Terpel y la forma de hacer efectivos sus
derechos, consultas y solicitudes de supresión de datos. Así mismo podrá consultar allí cualquier actualización a dicha política.
En virtud de la presente aceptación, voluntariamente cede y transere de manera irrevocable a Terpel los derechos patrimoniales de
autor sobre cualquier imagen y en general cualquier creación de propiedad intelectual que nazca, se desarrolle, se implemente, se
produzca, se cree con ocasión de la presente aceptación. Esta transferencia comprende todos los derechos patrimoniales respecto de
todas las modalidades de explotación de la obra conocidas o por conocerse, tiene vigencia por todo el tiempo de protección legal y
tiene efectos para el territorio mundial. Esta transferencia deja a salvo los derechos morales de autor, los que seguirán en cabeza de
quien suscribe el presente documento de manera que por este hecho no se genera ninguna obligación pecuniaria a cargo de
ORGANIZACIÓN TERPEL S.A.</p>

</div>

</div>
</div>
</div>

	<script src="js/jquery-3.3.1.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.validate.js"></script>
	<script src="js/messages_es.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/terpel-jquery.js"></script>
	<script type="text/javascript">
		$( document ).ready( function () {
			$( window ).on( "load", function () {


				<?
				if ( $msgTitle != "" ) { ?>
					$( '#msgModal' ).modal() 
				<? } ?>


				if ( $( window ).height() < $( window ).width() ) {

					if ( $( window ).height() > $( ".red-column" ).height() ) {
						//$( ".red-column" ).addClass( "align-items-center" );
						$( ".red-column" ).css( "height", $( window ).height() )
					} else {
						//$( ".red-column" ).removeClass( "align-items-center" );
					}




				} else {
					$( ".red-column" ).css( "height", $( window ).height() )
					//$( ".red-column" ).addClass( "align-items-center" );
				}


				$( ".pageLoader" ).fadeOut( "fast", function () {
					new WOW().init();
				} );


				setTimeout( function () {
					$( '#txtNit' ).addClass( 'shake' );
				}, 3000 );




			} );

			$( window ).on( "resize", function () {
				//$(".red-column").css("height", $(window).height() )

				if ( $( window ).height() < $( window ).width() ) {
					if ( $( window ).height() > $( ".red-column" ).height() ) {
						//$( ".red-column" ).addClass( "align-items-center" );
						$( ".red-column" ).css( "height", $( window ).height() )
					} else {
						$( ".red-column" ).removeClass( "align-items-center" );
					}
				} else {
					$( ".red-column" ).css( "height", $( window ).height() )
					//$( ".red-column" ).addClass( "align-items-center" );
				}
				console.log( "resize" );
				//$(".pageLoader").fadeOut("slow");
			} );

		} );
	</script>
</body>
</html>