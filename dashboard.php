<?php
session_start();
require('include/security.php');
require('include/utils.php');
require('include/mysql_class.php');
require_once 'vendor/autoload.php';

if($_SESSION['auth_register'] != 'true'){
    header("Location: index.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <title>Panel jefe de zona · Terpel</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="icon" href="images/icons/favicon.ico" type="image/x-icon">
  <meta property="og:type" content="website"/>
  <meta property="og:image" content="https://convenciondealiadosterpel.com/prereg/icons/apple-icon-180x180.png"/>
  <meta property="og:url" content="https://convenciondealiadosterpel.com/prereg"/>
  <meta property="og:title" content="Registro · Terpel"/>
  
  <?php include_once("analyticstracking.php") ?>
 <link rel="apple-touch-icon" sizes="57x57" href="icons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="icons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="icons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="icons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="icons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="icons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="icons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="icons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="icons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="icons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/icons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="images/icons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/icons/favicon-16x16.png">
<link rel="manifest" href="images/icons/manifest.json">
<meta name="msapplication-TileColor" content="#C30B13">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#C30B13">



  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/all.min.css">
  <link rel="stylesheet" href="css/terpel-fonts.css">
  <link rel="stylesheet" href="css/inmov.css">
  <link rel="stylesheet" href="css/animate.css">


<style type="text/css">
  
    body {    
      background-color: white !important;   
     } 
  

</style>

</head>
<body>

  

 <div class="pageLoader"></div>
 
<?php 
$selected_menu = 'dashboard';
include_once("header.php");
 ?>

<div class="container">
<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4 pl-0">
        <h1 class="page-title"><a class="text-danger px-1 disabled" href="dashboard.php"><i class="fa-fw fa fa-home"></i> Inicio</a> </span></h1>
    </div>
</div>

<div class="row" style="margin-bottom:40px;">

<?php /*?>
    <div class="col-xl-3 col-lg-3 col-3">
        <div class="card">
            <div class="card-content stat-card"  data-destination="reg=">
                <div class="media align-items-stretch">
                    <div class="p-2 text-center bg-primary align-middle">
                        <i class="fas fa-users font-large-2 white stat-icon"></i>
                    </div>
                    <div class="py-1 px-2 media-body">
                            <h5 class="text-primary stat-label">Pre registros</h5>
                            <h5 class="text-bold-400 stat-value "><span class="pre-value">0</span> de <span class="pre-uni">0</span></h5>
                            <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-primary pre-por" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php */?>

    <div class="col-xl-3 col-lg-3 col-3 col-md-12">
        <div class="card">
            <div class="card-content stat-card"  data-destination="reg="><?/* reg=ns */?>
                <div class="media align-items-stretch">
                    <div class="p-2 text-center bg-danger align-middle">
                        <i class="fas fa-user-times font-large-2 white stat-icon"></i>
                    </div>
                    <div class="py-1 px-2 media-body">
                            <h5 class="text-danger stat-label">EDS Pre-registradas</h5>
                            <h5 class="text-bold-400 stat-value"><span class="reg-value">0</span> de <span class="reg-uni-all">0</span></h5>
                            <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-primary reg-por" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                    </div>
                </div>
            <div class="card-footer" style="padding: 0.25rem 0.5rem 0.5rem 0.5rem !important"><small><b>EDS</b> pre-registradas y lista de aliados de su zona</small></div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-lg-3 col-3 col-md-12">
        <div class="card">
            <div class="card-content stat-card"  data-destination="reg=d">
                <div class="media align-items-stretch">
                    <div class="p-2 text-center bg-warning align-middle">
                        <i class="fas fa-user-clock font-large-2 white stat-icon"></i>
                    </div>
                    <div class="py-1 px-2 media-body">
                            <h5 class="text-warning stat-label">Registros Incompletos</h5>
                            <h5 class="text-bold-400 stat-value "><span class="rin-value">0</span> de <span class="reg-uni">0</span></h5>
                            <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-primary rin-por" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                    </div>
                </div>
            </div>
            <div class="card-footer" style="padding: 0.25rem 0.5rem 0.5rem 0.5rem !important"><small>Formularios iniciados sin finalizar</small></div>
        </div>
        
    </div>

    <div class="col-xl-3 col-lg-3 col-3 col-md-12">
        <div class="card">
            <div class="card-content stat-card"   data-destination="reg=c">
                <div class="media align-items-stretch">
                    <div class="p-2 text-center bg-primary align-middle">
                        <i class="fas fa-user-check font-large-2 white stat-icon"></i>
                    </div>
                    <div class="py-1 px-2 media-body">
                            <h5 class="text-primary stat-label">Registros Diligenciados</h5>
                            <h5 class="text-bold-400 stat-value "><span class="rco-value">0</span> de <span class="reg-uni">0</span></h5>
                            <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-primary rco-por" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                    </div>
                </div>
            </div>
            <div class="card-footer" style="padding: 0.25rem 0.5rem 0.5rem 0.5rem !important"><small>Formularios diligenciados sin firma y/o sin cargar</small></div>
        </div>
         
    </div>
    
    <div class="col-xl-3 col-lg-3 col-3 col-md-12">
        <div class="card">
            <div class="card-content stat-card"   data-destination="reg=f">
                <div class="media align-items-stretch">
                    <div class="p-2 text-center bg-success align-middle">
                        <i class="far fa-suitcase font-large-2 white stat-icon"></i>
                    </div>
                    <div class="py-1 px-2 media-body">
                            <h5 class="text-success stat-label">Finalizado</h5>
                            <h5 class="text-bold-400 stat-value "><span class="fin-value">0</span> de <span class="reg-uni">0</span></h5>
                            <div class="progress mt-1 mb-0" style="height: 7px;">
                                <div class="progress-bar bg-primary fin-por" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                    </div>
                </div>
            </div>
            <div class="card-footer" style="padding: 0.25rem 0.5rem 0.5rem 0.5rem !important "><small>Registros finalizados y cargados</small></div>
        </div>
        
    </div>


</div>


<div class="row"  style="margin-bottom:40px;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div id="pre_bars" style="width:100%; height:400px;"></div>
    </div>
    
 
</div>


<div class="row">    
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div id="pre_reg" style="width:100%; height:400px;"></div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div id="reg_stats" style="width:100%; height:400px;"></div>
    </div>
</div>





</div>

<script src="js/jquery-3.3.1.js"></script>  
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/messages_es.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/terpel-jquery.js"></script>
<script src="Highcharts/code/highcharts.js"></script>
<script src="Highcharts/code/modules/exporting.js"></script>
<script src="Highcharts/code/modules/export-data.js"></script>

<script type="text/javascript">
	function graph_pie(target, title, name, values) {

		Highcharts.chart(target, {     
            credits: false,
            exporting:{
                enabled:false
            },
            colors: ['#A80C00', '#F4DA40', '#B3B3B3', '#EAEAEA'],
			chart: {
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false,
				type: 'pie'
			},
			title: {
				text: title
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					dataLabels: {
						enabled: false
					},
					showInLegend: true
				}
			},
			series: [{
				name: name,
				data: values,
				colorByPoint: true,

			}]
		});

	}

    function graph_bars(target, title, xvalues, xtitle, yvalues, ytitle, color="#509EE3"){

            Highcharts.chart(target, {  
            credits: false,
            exporting:{
                enabled:false
            },
            colors: ['#A80C00', '#F4DA40', '#B3B3B3', '#EAEAEA'],
            chart: {
             type: 'column'
            },
            title: {
                text: title
            },
            xAxis: {
            categories: xvalues,
            crosshair: true,
            /*labels: {
                rotation: 20
            }*/
                title: {
                    text: xtitle
                }
            },
            yAxis: {
            min: 0,
            title: {
                text: ytitle
            }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.2f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
            },
            series: yvalues
            
            });

}

$(document).ready(function() {
<?php 
    $sql = 'SELECT COUNT(u.id) AS cant FROM `USER` AS u
            INNER JOIN `GASSTATION` AS g ON u.`id` = g.`USER_id`
            LEFT JOIN `GASSTATIONREG` AS gr ON u.`id` = gr.`USER_id`
            WHERE gr.`id` IS NULL AND g.`ZONELEADER_id` = '.$_SESSION["id"];

    $micon->query($sql);
    $habilitados = $micon->fetchArray()["cant"];

    $sql = 'SELECT COUNT(gr.id) as cant FROM  GASSTATIONREG as gr
            INNER JOIN `GASSTATION` as g ON gr.`GASSTATION_id` = g.`id`
            WHERE g.`ZONELEADER_id` = '.$_SESSION["id"];

    $micon->query($sql);
    $pre_reg = $micon->fetchArray()["cant"];



    $sql ="SELECT COUNT(*) AS cant, 'Total EDS' AS tipo FROM `GASSTATION` g
            INNER JOIN `ZONELEADER` ON g.`ZONELEADER_id` = `ZONELEADER`.`id`
            WHERE g.`ZONELEADER_id` = '".$_SESSION[id]."'
            UNION
            SELECT COUNT(r.`document`) AS cant, 'Pre-registro' AS tipo
                FROM  `registration` AS r
                INNER JOIN `GASSTATION` AS g ON r.`gasstation_id` = g.`id`
                WHERE g.`ZONELEADER_id` =  '".$_SESSION[id]."' AND `status_reg` != 'f'
            UNION
            SELECT COUNT(r.`document`) AS cant, 'Finalizado' AS tipo
                FROM  `registration` AS r
                INNER JOIN `GASSTATION` AS g ON r.`gasstation_id` = g.`id`
                WHERE g.`ZONELEADER_id` =  '".$_SESSION[id]."' AND `status_reg` = 'f'";
    $micon->query($sql);
    $cant1 = array();
    $desc = array();
    while($item = $micon->fetchArray()){
        array_push($cant1, $item[cant]);
        array_push($desc, $item[tipo]);

    }

    $values_n = '';
    foreach ($cant1 as $k => $v) {
            $tmp_cat .= "'Estado',";
            $tmp_val .= "$v,";
            $values_n .= "
                {
                    name: '".$desc[$k]."',
                    data: [".$v."],
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}',
                        style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },";
        }

        

    $tmp_cat = rtrim($tmp_cat, ',');
    $tmp_val = rtrim($tmp_val, ',');
    $values_n = rtrim($values_n, ',');
    
    ?>
    categories = [<?php echo $tmp_cat ?>]
    values = [<?php echo $tmp_val ?>]

		values1 = [
                  {'name': 'Usuarios Sin Pre Registro', 'y': <?=(!empty($habilitados))?$habilitados:'0';?>},
                  {'name': 'Pre registrados', 'y':  <?=(!empty($pre_reg))?$pre_reg:'0';?>}
                 ];

        graph_pie('pre_reg', "Usuarios Pre Registrados ", "Porcentaje", values1);  
           
        $(".pre-uni").empty().text("<?=(!empty($habilitados))?$habilitados:'0';?>");  
        $(".pre-value").empty().text("<?=(!empty($pre_reg))?$pre_reg:'0';?>");
        var por = ((<?=$pre_reg?>  * 100) / <?=$habilitados?>);
        $(".pre-por").attr('aria-valuenow', por.toFixed(1) );


        var values  = [<?php echo $values_n?>];
        
        graph_bars('pre_bars','EDS por estado',categories,'',values,"Cantidad de EDS",  "#da291c");
        
<?php 

    $sql = 'SELECT COUNT(r.`document`) AS cant FROM  `registration` AS r
    INNER JOIN `GASSTATION` AS g ON r.`gasstation_id` = g.`id`
    WHERE g.`ZONELEADER_id` = '.$_SESSION["id"];
   // $sql = "SELECT COUNT(*) AS cant  FROM `GASSTATION` WHERE `ZONELEADER_id` = '$_SESSION[id]'";
  //  echo $sql;
    $micon->query($sql);
    $reg_cant = $micon->fetchArray()["cant"];

    $sqlQty = "SELECT COUNT(*) AS cant  FROM `GASSTATION` WHERE `ZONELEADER_id` = '$_SESSION[id]'";

    //echo $sql;
    $micon->query($sqlQty);
    $qtyPrereg = $micon->fetchArray()["cant"];



    $sql = "SELECT COUNT(rg.`id`) AS cant FROM `GASSTATIONREG` as rg
            INNER JOIN `GASSTATION` AS g ON rg.`GASSTATION_id` = g.`id`
            WHERE rg.response = 'yes' AND g.`ZONELEADER_id` = ".$_SESSION["id"];
    $micon->query($sql);
    $yes = $micon->fetchArray()["cant"];

    $sql = "SELECT COUNT(rg.`id`) AS cant FROM `GASSTATIONREG` as rg
            INNER JOIN `GASSTATION` AS g ON rg.`GASSTATION_id` = g.`id`
            WHERE rg.response = 'no' AND g.`ZONELEADER_id` = ".$_SESSION["id"];
    $micon->query($sql);
    $no = $micon->fetchArray()["cant"];
		?>
		values = [
                  {'name': 'Si', 'y': <?=(!empty($yes))?$yes:'0';?>},
                  {'name': 'No', 'y':  <?=(!empty($no))?$no:'0';?>}
                 ];
   
    //graph_pie('reg_cant', "Usuarios Registrados", "Porcentaje", values);
    $(".reg-uni").empty().text("<?=(!empty($reg_cant))?$reg_cant:'0';?>"); 
    $(".reg-uni-all").empty().text("<?=(!empty($qtyPrereg))?$qtyPrereg:'0';?>"); 


<?php 
    $sql = "SELECT COUNT(r.`document`) AS cant, r.`status_reg` AS `status` 
    FROM  `registration` AS r
    INNER JOIN `GASSTATION` AS g ON r.`gasstation_id` = g.`id`
    WHERE g.`ZONELEADER_id` =  '".$_SESSION["id"]."'
    GROUP BY r.`status_reg`";
    $micon->query($sql);
    
    $tmp_val = '';
    $universe = 0;
	while($result = $micon->fetchArray()) {
        $name = '';
        $universe += $result['cant'];
        switch ($result['status']){
            /*case 'ns':
                $name = 'Sin visitar';
                echo '$(".reg-value").empty().text("'.$result['cant'].'");
                      var por2 = (('.$result['cant'].'  * 100) / '.$qtyPrereg.');
                      $(".reg-por").attr("aria-valuenow", por2.toFixed(1) );';

            break;*/
            case 'd':
                $name = 'Borrador';
                echo '$(".rin-value").empty().text("'.$result['cant'].'");
                     var por3 = (('.$result['cant'].'  * 100) / '.$reg_cant.');
                     $(".rin-por").attr("aria-valuenow", por3.toFixed(1) );';
            break;
            case 'f':
                $name = 'Finalizado';
                echo '$(".fin-value").empty().text("'.$result['cant'].'");
                     var por4 = (('.$result['cant'].'  * 100) / '.$reg_cant.');
                     $(".fin-por").attr("aria-valuenow", por4.toFixed(1) );';
            break;
            default: //c
                $name = 'Completo';
                echo '$(".rco-value").empty().text("'.$result['cant'].'");
                      var por4 = (('.$result['cant'].'  * 100) / '.$reg_cant.');
                      $(".rco-por").attr("aria-valuenow", por4.toFixed(1) );';
            break;
        }
            
            $tmp_val .= "{'name': '" . $name . "', 'y': ".$result['cant']."},";
            

            
		}
          $name = 'Sin visitar';
         $tmp_val .= "{'name': '" . $name . "', 'y': ".$qtyPrereg."},";

         echo "\n" . '$(".reg-value").empty().text("'.$universe.'");
          var por2 = (('.$universe.'  * 100) / '.$qtyPrereg.');
          $(".reg-por").attr("aria-valuenow", por2.toFixed(1) );';

		$tmp_val = rtrim($tmp_val, ',');

		?>
		values2 = [<?php echo $tmp_val ?>]
    
   
    graph_pie('reg_stats', "Estado de Registros", "Porcentaje", values2);


    
        $(".stat-card").on('click', function(){
            var destination = $(this).attr('data-destination');
            window.location.href = 'list.php?'+destination;
        });  

  });
  
  $(window).on("load",function() {
      $(".pageLoader").fadeOut("fast", function() {
        setTimeout(function(){
            $('.progress .progress-bar').css("width",
                function() {
                    return $(this).attr("aria-valuenow") + "%";
                }
            )
        }, 500);
        new WOW().init();
       });
    });

</script>


</body>
</html>