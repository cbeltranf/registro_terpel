<?
session_start();
//require('include/security.php');
require('include/utils.php');
require('include/mysql_class.php');

$id = satinize_me($_GET['id'], 'int');  
$zone = satinize_me($_GET['zone'], 'int');  
$sql = "SELECT
            r.*
        FROM
            `registration` AS r
                INNER JOIN `GASSTATION` AS g ON r.`gasstation_id` = g.`id`
        WHERE g.`ZONELEADER_id` =  '".$zone."' AND r.document='$id'";

$micon->query( $sql );
$regInfo = $micon->fetchArray();
$count = $micon->numRows();

if($count > 0){

$sql = "SELECT
    *
FROM
    `companions`
    WHERE registration_id='$id'";

$micon->query( $sql );

$dataCompa = array();


while($regCompa = $micon->fetchArray()){
    $objCompa                  = new stdClass();
    $objCompa->name            = $regCompa[ name ];
    $objCompa->type            = $regCompa[ type ];
    $objCompa->age             = $regCompa[ age ];
    $objCompa->passport_number = $regCompa[ passport_number ];
    $objCompa->have_passport   = $regCompa[ have_passport ];
    $objCompa->passport_image  = $regCompa[ passport_image ];

    array_push( $dataCompa, $objCompa );
}
?><!DOCTYPE html>
<html lang="es">
<head>
  <title>PDF · Terpel</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/all.min.css">
  <link rel="stylesheet" href="css/terpel-fonts.css">
  <link rel="stylesheet" href="css/inmov.css">
  <link rel="stylesheet" href="css/animate.css">

<style type="text/css">  
    body {    
      background-color: white !important;   
     } 
    
  body{
    font-size: 0.8rem;
  }
  .text-center{
        text-align: center;
            }
   table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > td:first-child:before, table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > th:first-child:before{
        margin-top: 2px; 
   }
   .page-title{
        letter-spacing: -1px;
        font-size: 0.9rem;
        margin: 14px 0 25px;
   }
  .tab-pane{
    border: 1px solid #dee2e6;
    padding: 5px;
  }
  .dtr-data{
    padding-left: 14px;
  }
  .control a:not(:first-child) {
    margin-left: 10px;
  }
  .navbar-dark .navbar-nav .nav-link {
    color: rgba(255,255,255,.7);
}
.btn-group{
  position: absolute;
}

.btn-group .btn{
  font-size: 0.7rem;
}

.font-large-2 {
    font-size: 3rem!important;
}

.white {
    color: #FFF!important;
}
.text-bold-400 {
  font-weight: 700;
} 
.stat-value{
  font-size: 1rem;  
  padding: 0 10px 5px 10px;
}
.stat-label{
  font-size: 0.7rem;
  padding: 5px 10px 0 10px;
  font-weight: 700;
}
.stat-icon{
  margin-top: 5px;
}
.progress-bar {
  text-align: left;
  transition-duration: 2s;
} 
.stat-card{
  cursor:pointer;
}

.page-item.active .page-link {
    z-index: 1;
    color: #fff;
    background-color: #dc3545!important;
    border-color: #dc3545!important;
}
.page-link{
  color:#dc3545;
}
.page-item.active .page-link {
  
  color:#fff !important;
}

</style>
</head>
<body>
  
<div class="container">


<div class="row">




<div class="card " style="width: 100%;">  
  <div class="card-header card-header text-white " style="background-color: #E1251B">
  
<h5 class="m-0 p-0 tt_normsbold">1. Información general</h5>
</div>
    

   <div class="card-body card-body p-0">

         
            <ul class="list-group list-group-flush">
            <li class="list-group-item">

            <div class="form-row">
                        <div class="form-group col-md-8">
                            <label for="txtFullName" class="tt_normsmedium">Nombres y Apellidos Completos <small style="display: inherit;" id="txtFullNameHelp" class="form-text text-muted">
                            Como figura en el pasaporte
                          </small></label>
                            <input id="txtFullName" required type="text" class="form-control input-lg tt_normslight"  name="txtFullName" aria-describedby="txtFullNameHelp" minlength="5" value="<?=$regInfo[name] . " " . $regInfo[last_name] ?>"  autocomplete="off" onclick="this.select();">

                            

                        </div>


                      <div class="form-group col-md-4">
                            <label for="txtDoc" class="tt_normsmedium">Número de Cédula</label>
                            <input id="txtDoc" required type="number" value="<?=$regInfo[document] ?>" class="form-control input-lg tt_normslight"  name="txtDoc" minlength="5"  autocomplete="off" onclick="this.select();">
                        </div>

                      </div>

                      <div class="form-row">

                        <div class="form-group col-md-4  ">
                            <label for="rdoHasPassPort0" class="tt_normsmedium">¿Tiene pasaporte?</label>

                            <div class="c-inputs-stacked p-0 pt-2">
                            <label class="c-input c-radio pr-3 ">
                            <input class="required"  autocomplete="off" name="rdoHasPassPort0" type="radio" value="yes" <?=($regInfo[has_passport]=='yes') ? "checked" : "" ?> >
                            <span class="c-indicator pl-2"></span> Sí </label>
                          
                            <label class="c-input c-radio">
                            <input class="required" autocomplete="off"  name="rdoHasPassPort0" type="radio" value="no" <?=($regInfo[has_passport]=='no') ? "checked" : "" ?>>
                            <span class="c-indicator pl-2"></span> No </label>
                          
                          </div>
                        </div>


                        <div class="form-group col-md-4">
                           <label for="txtPassport0" class="tt_normsmedium">No. Pasaporte</label>
                            <input autocomplete="off" <?=($regInfo[has_passport]=='yes') ? "required" : "" ?>  id="txtPassport0" type="text" class="form-control input-lg tt_normslight "  name="txtPassport0" <?=($regInfo[has_passport]=='no') ? "disabled" : "" ?>  value="<?=$regInfo[passport_number] ?>"  onclick="this.select();">

                        </div>    

                        <div class="form-group col-md-4 pt-4">
                            
                        <label title="Subir pasaporte" id="label-file-passPort0" for="file-passPort0"  <?=($regInfo[has_passport]=='no') ? "disabled" : "" ?> class="btn btn btn-secondary <?=($regInfo[has_passport]=='no') ? "disabled" : "" ?>" style="margin-bottom: 0;">
                         <i class="fal fa-upload"></i></label>
                        <input type="file" accept="image/jpeg,application/pdf" <?=($regInfo[has_passport]=='no') ? "disabled" : "" ?> name="file" id="file-passPort0" class="d-none btnUpload">

                        <a  target="_blank" href="<?=($regInfo[passport_file]!="") ? URL . "travel_docs/" . $regInfo[passport_file] : "#" ?>" class="btn btn-secondary <?=($regInfo[passport_file]=="") ? "disabled" : "" ?>">
                        <i class="fal fa-eye"></i></a> 
                            

                        </div>                    

                      </div>


                      <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="txtBDay" class="tt_normsmedium">Fecha de nacimiento</label>
                            <input  required id="txtBDay" type="date" class="form-control input-lg tt_normslight" value="<?=$regInfo[birthday] ?>"  name="txtBDay"  onclick="this.select();">

                        </div>
                      <div class="form-group col-md-4">
                            <label for="txtEMail" class="tt_normsmedium">Correo electrónico</label>
                            <input required id="txtEMail" type="email" class="form-control input-lg tt_normslight"  value="<?=$regInfo[email] ?>"  name="txtEMail"  onclick="this.select();">

                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="txtCity" class="tt_normsmedium">Ciudad</label>
                            <input required id="txtCity" type="text" class="form-control input-lg tt_normslight" value="<?=$regInfo[city] ?>"  name="txtCity"  onclick="this.select();">

                        </div>

                      </div>


                      <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="txtBussiness" class="tt_normsmedium">Razón social</label>
                            <input required id="txtBussiness" type="text" class="form-control input-lg tt_normslight"  value="<?=$regInfo[bussiness] ?>"  name="txtBussiness"  onclick="this.select();">

                        </div>
                      <div class="form-group col-md-4">
                            <label for="txtNit" class="tt_normsmedium">Nit</label>
                            <input required id="txtNit" type="number" class="form-control input-lg tt_normslight"  value="<?=$regInfo[nit] ?>" name="txtNit" onclick="this.select();">

                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="txtEDS" class="tt_normsmedium">Nombre Completo EDS</label>
                            <input required id="txtEDS" type="text" class="form-control input-lg tt_normslight" value="<?=$regInfo[eds_name] ?>" name="txtEDS" onclick="this.select();">

                        </div>

                      </div>




                      <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="txtRegionalName" class="tt_normsmedium">Regional</label>
                            <input required id="txtRegionalName" type="text" class="form-control input-lg tt_normslight" value="<?=$regInfo[regional] ?>" name="txtRegionalName" onclick="this.select();">

                        </div>
                      <div class="form-group col-md-4">
                            <label for="txtSapCpde" class="tt_normsmedium">Código SAP</label>
                            <input required id="txtSapCpde" type="text" class="form-control input-lg tt_normslight" value="<?=$regInfo[sap_code] ?>" name="txtSapCpde"  onclick="this.select();">

                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="txtEDS" class="tt_normsmedium">Código Único</label>
                            <input required id="txtEDS" type="text" class="form-control input-lg tt_normslight" value="<?=$regInfo[unique_code] ?>" name="txtEDS"  onclick="this.select();">

                        </div>

                      </div>
                    


                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="txtAdress" class="tt_normsmedium">Dirección</label>
                            <input required id="txtAdress" type="text" class="form-control input-lg tt_normslight" value="<?=$regInfo[adress] ?>"  name="txtAdress"  onclick="this.select();">

                        </div>
                      <div class="form-group col-md-4">
                            <label for="txtMobilePhone" class="tt_normsmedium">Número de Celular</label>
                            <input required id="txtMobilePhone" type="tel" class="form-control input-lg tt_normslight" value="<?=$regInfo[cellphone] ?>"  name="txtMobilePhone" onclick="this.select();">

                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="txtLandPhone" class="tt_normsmedium">Telefono Fijo</label>
                            <input required id="txtLandPhone" type="tel" class="form-control input-lg tt_normslight"  value="<?=$regInfo[phone] ?>" name="txtLandPhone"  onclick="this.select();">

                        </div>

                      </div>


          </li>


            <li class="list-group-item passport-space " id="liInfoCompanion"  >
              
              <div class="alert alert-info alert-dismissible fade show tt_normsmedium" role="alert">
              <i class="fa-info-circle fal"></i> <b> Identifica las personas</b> que viajarán a la Convención TERPEL 2019 y la acomodación deseada. Recuerda que cada una de las personas debe firmar la autorización de manejo de datos que se encuentra adjunta (Anexo No.1) al presente documento. Si es un menor de edad, entonces la autorización deberá ser firmada por su acudiente.
               
              </div>    


            </li>
            <li class="list-group-item passport-space " id="liCompanion1" style=" border-top: 0; padding-top: 0;">

                <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="txtCompanion1"  class="tt_normsmedium">Nombre Completo acompañante No 1</label>
                            <input id="txtCompanion1" value="<?=$dataCompa[0]->name ?>" type="text" class="form-control input-lg tt_normslight"  name="txtCompanion1" aria-describedby="txtFullNameHelp" minlength="5"  autocomplete="off" onclick="this.select();">                           

                        </div>


                      

                      </div>




                      <div class="form-row">

                        <div class="form-group col-md-4  ">
                            <label for="rdoHasPassPort1"  class="tt_normsmedium">¿Tiene pasaporte?</label>

                            <div class="c-inputs-stacked p-0 pt-2">
                            <label class="c-input c-radio pr-3 ">
                            <input class=""  autocomplete="off" <?=($dataCompa[0]->have_passport=='S') ? "checked" : "" ?>  name="rdoHasPassPort1" type="radio" value="S" >
                            <span class="c-indicator pl-2"></span> Sí </label>
                          
                            <label class="c-input c-radio">
                            <input class="" autocomplete="off"  <?=($dataCompa[0]->have_passport=='N') ? "checked" : "" ?> name="rdoHasPassPort1" type="radio" value="N">
                            <span class="c-indicator pl-2"></span> No </label>
                          
                          </div>
                        </div>


                        <div class="form-group col-md-4 ">
                           <label for="txtPassport1"  class="tt_normsmedium">No. Pasaporte</label>
                            <input  id="txtPassport1" <?=($dataCompa[0]->have_passport=='N') ? "disabled" : "" ?> value="<?=$dataCompa[0]->passport_number ?>" type="text" class="form-control input-lg tt_normslight"  name="txtPassport1"  onclick="this.select();">

                        </div>  


                        <div class="form-group col-md-4 pt-4">
                            
                        <label title="Subir pasaporte" for="file-passPort1" id="label-file-passPort1" <?=($dataCompa[0]->have_passport=='N') ? "disabled" : "" ?> class="btn btn btn-secondary <?=($dataCompa[0]->have_passport=='N') ? "disabled" : "" ?>" style="margin-bottom: 0;">
                         <i class="fal fa-upload"></i></label> 
                        <input type="file" <?=($dataCompa[0]->have_passport=='N') ? "disabled" : "" ?> accept="image/jpeg,application/pdf" name="file" id="file-passPort1" class="d-none btnUpload">

                        <a target="_blank" href="<?=($dataCompa[0]->passport_image!="") ? URL . "travel_docs/" . $dataCompa[0]->passport_image : "#" ?>" class="btn btn-secondary <?=($dataCompa[0]->passport_image=="") ? "disabled" : "" ?>">
                        <i class="fal fa-eye"></i></a> 
                            

                        </div>  




                      </div>

                      <div class="form-row">

                        <div class="form-group col-md-4  ">
                            <label for="rdoAgeRange1" class="tt_normsmedium">¿Es adulto o menor de edad?</label>
                            <div class="c-inputs-stacked p-0 pt-2">
                            <label class="c-input c-radio pr-3 ">
                            <input class="" <?=($dataCompa[0]->type=='Adulto') ? "checked" : "" ?>  autocomplete="off" id="si" name="rdoAgeRange1" type="radio" value="Adulto" >
                            <span class="c-indicator pl-2"></span> Adulto </label>
                          
                            <label class="c-input c-radio">
                            <input class="" <?=($dataCompa[0]->type=='Menor') ? "checked" : "" ?>  autocomplete="off" id="no" name="rdoAgeRange1" type="radio" value="Menor">
                            <span class="c-indicator pl-2"></span> Menor de edad </label>
                          
                          </div>

                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="txtAge1" class="tt_normsmedium">Si es menor de edad especifique la edad</label>
                            <input id="txtAge1" <?=($dataCompa[0]->type=='Adulto') ? "disabled" : "" ?> value="<?=$dataCompa[0]->age ?>" type="number" min="0" max="17" class="form-control input-lg tt_normslight"  name="txtAge1"  onclick="this.select();">

                        </div>

                         

                      </div>
                      
                     
            </li>

             <li class="list-group-item passport-space " id="liCompanion2">
                  <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="txtCompanion2" class="tt_normsmedium">Nombre Completo acompañante No 2</label>
                            <input id="txtCompanion2" value="<?=$dataCompa[1]->name ?>"  type="text" class="form-control input-lg tt_normslight"  name="txtCompanion2" aria-describedby="txtFullNameHelp" minlength="5"  autocomplete="off" onclick="this.select();">

                           

                        </div>


                      

                      </div>




                      <div class="form-row">

                        <div class="form-group col-md-4  ">
                            <label for="rdoHasPassPort2" class="tt_normsmedium">¿Tiene pasaporte?</label>

                            <div class="c-inputs-stacked p-0 pt-2">
                            <label class="c-input c-radio pr-3 ">
                            <input class=" "  autocomplete="off" <?=($dataCompa[1]->have_passport=='S') ? "checked" : "" ?>  name="rdoHasPassPort2" type="radio" value="S" >
                            <span class="c-indicator pl-2"></span> Sí </label>
                          
                            <label class="c-input c-radio">
                            <input class="" autocomplete="off"  <?=($dataCompa[1]->have_passport=='N') ? "checked" : "" ?> name="rdoHasPassPort2" type="radio" value="N">
                            <span class="c-indicator pl-2"></span> No </label>
                          
                          </div>
                        </div>


                        <div class="form-group col-md-4">
                           <label for="txtPassport2" class="tt_normsmedium">No. Pasaporte</label>
                            <input  id="txtPassport2" type="text" class="form-control input-lg tt_normslight" <?=($dataCompa[1]->have_passport=='N') ? "disabled" : "" ?> value="<?=$dataCompa[1]->passport_number ?>"  name="txtPassport2"  onclick="this.select();">

                        </div>    

                        <div class="form-group col-md-4 pt-4">
                            
                        <label title="Subir pasaporte" id="label-file-passPort2" <?=($dataCompa[1]->have_passport=='N') ? "disabled" : "" ?> for="file-passPort2" class="btn btn btn-secondary <?=($dataCompa[1]->have_passport=='N') ? "disabled" : "" ?>" style="margin-bottom: 0;">
                         <i class="fal fa-upload"></i></label>
                        <input <?=($dataCompa[1]->have_passport=='N') ? "disabled" : "" ?> type="file" accept="image/jpeg,application/pdf"  name="file" id="file-passPort2" class="d-none btnUpload">

                        <a target="_blank" href="<?=($dataCompa[1]->passport_image!="") ? URL . "travel_docs/" . $dataCompa[1]->passport_image : "#" ?>" class="btn btn-secondary <?=($dataCompa[1]->passport_image=="") ? "disabled" : "" ?>">
                        <i class="fal fa-eye"></i></a> 
                            

                        </div>                    

                      </div>

                      <div class="form-row">

                        <div class="form-group col-md-4  ">
                            <label for="rdoAgeRange2" class="tt_normsmedium">¿Es adulto o menor de edad?</label>
                            <div class="c-inputs-stacked p-0 pt-2">
                            <label class="c-input c-radio pr-3 ">
                            <input class="" <?=($dataCompa[1]->type=='Adulto') ? "checked" : "" ?> autocomplete="off" id="si" name="rdoAgeRange2" type="radio" value="Adulto" >
                            <span class="c-indicator pl-2"></span> Adulto </label>
                          
                            <label class="c-input c-radio">
                            <input class="" <?=($dataCompa[1]->type=='Menor') ? "checked" : "" ?> autocomplete="off" id="no" name="rdoAgeRange2" type="radio" value="Menor">
                            <span class="c-indicator pl-2"></span> Menor de edad </label>
                          
                          </div>

                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="txtAge2" class="tt_normsmedium">Si es menor de edad especifique la edad</label>
                            <input id="txtAge2" <?=($dataCompa[1]->type=='Adulto') ? "disabled" : "" ?> value="<?=$dataCompa[1]->age ?>" min="0" max="17" type="number" class="form-control input-lg tt_normslight"  name="txtAge2"  onclick="this.select();">

                        </div>

                      </div>
             </li>


          </ul>

                      


                       
             

              



              

      





  </div>
    <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
  </div>

<!--------------------------------------->

<div class="card mt-3" style="width: 100%;">  
  <div class="card-header card-header text-white " style="background-color: #E1251B">
  
<h5 class="m-0 p-0 tt_normsbold">1.1 Tipo de acomodación</h5>
</div>
    

   <div class="card-body card-body ">
        <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="txtNRoomSimple" class="tt_normsmedium">No de habitaciones Sencilla</label>
                            <input value="<?=$regInfo[simple_room]?>" required id="txtNRoomSimple" type="number" class="form-control input-lg tt_normslight"  name="txtNRoomSimple"  onclick="this.select();">

                        </div>
                      <div class="form-group col-md-4">
                            <label for="txtNRoomDouble" class="tt_normsmedium">No de habitaciones Doble</label>
                            <input value="<?=$regInfo[double_room]?>"  required id="txtNRoomDouble" type="number" class="form-control input-lg tt_normslight"  name="txtNRoomDouble"  onclick="this.select();">

                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="txtNRoomTriple" class="tt_normsmedium">No de habitaciones Triple</label>
                            <input value="<?=$regInfo[triple_room]?>"  required id="txtNRoomTriple" type="number" class="form-control input-lg tt_normslight"  name="txtNRoomTriple"  onclick="this.select();">

                        </div>

                      
                    </div>
  </div>
    
  </div>



  <div class="card mt-3" style="width: 100%;">  
  <div class="card-header card-header text-white " style="background-color: #E1251B">
  
<h5 class="m-0 p-0 tt_normsbold">1.2. Extensión (Pre o Post)</h5>
</div>
    

   <div class="card-body card-body ">
        <div class="form-row">

                        <div class="form-group col-md-4">
                             <label for="rdoExtension" class="tt_normsmedium">¿Tomarás extension (Pre o Post)?</label>
                              <div class="c-inputs-stacked p-0 pt-2">
                              <label class="c-input c-radio pr-3 ">
                              <input required autocomplete="off" <?=($regInfo["extension_detail"] == "yes") ? "checked" : "" ?> name="rdoExtension" type="radio" value="yes" >
                              <span class="c-indicator pl-2"></span> Si </label>
                            
                              <label class="c-input c-radio">
                              <input required autocomplete="off" <?=($regInfo["extension_detail"] == "no") ? "checked" : "" ?> name="rdoExtension" type="radio" value="no">
                              <span class="c-indicator pl-2"></span> No </label>
                            
                            </div>

                        </div>
                      <div class="form-group col-md-8">
                            
				<label class="" for="slExtension">¿Cuál?</label>

                           <select <?=($regInfo["extension_detail"] != "yes") ? "disabled" : "" ?> autocomplete="off" class="c-select form-control form-control-md" style="width:100%;" id="slExtension" name="slExtension" aria-describedby="helpMap" >
							                  <option selected value="">Seleccione una</option>
							                  <?php 
												  $sql = "SELECT
    `id`
    , `extension_name`
FROM
    `EXTENSIONS` order by extension_name asc";
												
												$micon->query($sql);
												while($ext = $micon->fetchArray()){
											  ?>
							                  <option <?=($regInfo["extension"] == $ext["id"]) ? "selected" : "" ?> value="<?=$ext["id"]?>"><?=$ext["extension_name"]?></option>
							                   <? } ?>
							                </select>

                        </div>
                        
                        

                      
                    </div>
  </div>
    
  </div>


<!--------------------------------------->

  <div class="card mt-3" style="width: 100%;">  
  <div class="card-header card-header text-white " style="background-color: #E1251B">
  
<h5 class="m-0 p-0 tt_normsbold">2. Indique su forma de pago</h5>
</div>
    

   <div class="card-body card-body ">
        

        <div class="alert alert-info alert-dismissible fade show tt_normsmedium" role="alert">
              <i class="fa-info-circle fal"></i> <b> Lee antes</b> las condiciones aplicables a cada forma de pago y marca tu selección:
               
        </div>

          
        
        <div class="form-row">

                      <div class="form-group col-md-5 ">   
                       <label  class="tt_normsmedium">2.1</label> 
                      <div class="c-inputs-stacked p-0 " style="    margin-top: 12px;">
                          <label class="c-input c-radio pr-2 ">
                              <input <?=($regInfo["payment"] == "21") ? "checked" : "" ?>  class=""  autocomplete="off"  name="rdoPayment" type="radio" value="21" >
                              <span class="c-indicator pl-2"></span>  Pago de contado hasta el 31/08/2019 (Descuento del 9%)
                          </label>
                            
                             
                            
                      </div>

                      </div>

                      <div class="form-group col-md-7">
                            <label for="txt21" class="tt_normsmedium">$</label>  
                            <input <?=($regInfo["payment"] != "21") ? "disabled" : "" ?> id="txt21" type="number" class="form-control input-lg tt_normslight" value="<?=($regInfo["payment"] == "21") ? $regInfo["money"] : "" ?>"  name="txt21"  onclick="this.select();">

                      </div>
                        
                      
                      
          </div>

          <div class="form-row">

                      <div class="form-group col-md-5 ">   
                      <label  class="tt_normsmedium">2.2</label> 
                      <div class="c-inputs-stacked p-0 " >
                          <label class="c-input c-radio pr-3 pt-2">
                              <input <?=($regInfo["payment"] == "22") ? "checked" : "" ?>  class=""  autocomplete="off"  name="rdoPayment" type="radio" value="22" >
                              <span class="c-indicator pl-2"></span>  Pago hasta el 20/12/2019 (Descuento del 5%) 
                          </label>
                            
                             
                            
                      </div>

                      </div>
                      
                      <div class="form-group col-md-2">
                            <label for="txtQ22" class="tt_normsmedium">No Cuotas</label>  
                            <input <?=($regInfo["payment"] != "22") ? "disabled" : "" ?> id="txtQ22" type="number" class="form-control input-lg tt_normslight" value="<?=($regInfo["payment"] == "22") ? $regInfo["quotes"] : "" ?>"  name="txtQ22"  onclick="this.select();">

                      </div>

                      <div class="form-group col-md-5">
                            <label for="txtMoney22" class="tt_normsmedium">$</label>  
                            <input <?=($regInfo["payment"] != "22") ? "disabled" : "" ?> id="txtMoney22" type="number" class="form-control input-lg tt_normslight" value="<?=($regInfo["payment"] == "22") ? $regInfo["money"] : "" ?>" name="txtMoney22"  onclick="this.select();">

                      </div>
                                                                    
          </div>

          <div class="form-row">

                      <div class="form-group col-md-5 ">   
                      <label  class="tt_normsmedium">2.3</label> 
                      <div class="c-inputs-stacked p-0 " >
                          <label class="c-input c-radio pr-3 pt-2">
                              <input <?=($regInfo["payment"] == "23") ? "checked" : "" ?>  class=""  autocomplete="off"  name="rdoPayment" type="radio" value="23" >
                              <span class="c-indicator pl-2"></span>  Pago a cuotas (hasta el 30 abril 2021) 
                          </label>
                            
                             
                            
                      </div>

                      </div>
                      
                      <div class="form-group col-md-2">
                            <label for="txtQ23" class="tt_normsmedium">No Cuotas</label>  
                            <input <?=($regInfo["payment"] != "23") ? "disabled" : "" ?> id="txtQ23" type="number" class="form-control input-lg tt_normslight" value="<?=($regInfo["payment"] == "23") ? $regInfo["quotes"] : "" ?>" name="txtQ23"  onclick="this.select();">

                      </div>

                      <div class="form-group col-md-5">
                            <label for="txtMoney23" class="tt_normsmedium">$</label>  
                            <input <?=($regInfo["payment"] != "23") ? "disabled" : "" ?>  id="txtMoney23" type="number" class="form-control input-lg tt_normslight" value="<?=($regInfo["payment"] == "23") ? $regInfo["money"] : "" ?>" name="txtMoney23"  onclick="this.select();">

                      </div>
                                                                    
          </div>


           <div class="form-row">

                                            
                      <div class="form-group col-md-6">
                            <label  for="txtStartQuotingDate" class="tt_normsmedium">Fecha de inicio de cobro de la cuota (DD/MM/AA)</label>  
                            <input <?=($regInfo["payment"] == "21") ? "disabled" : "" ?> required id="txtStartQuotingDate" type="date" class="form-control input-lg tt_normslight"  name="txtStartQuotingDate" value="<?=$regInfo["start_quotes_date"]?>" onclick="this.select();">

                      </div>

                      <div class="form-group col-md-6">
                            <label for="txtFinishQuotingDate" class="tt_normsmedium">Fecha de fin de cobro de la cuota (DD/MM/AA)</label>  
                            <input <?=($regInfo["payment"] == "21") ? "disabled" : "" ?> required id="txtFinishQuotingDate" type="date" class="form-control input-lg tt_normslight"  name="txtFinishQuotingDate" value="<?=$regInfo["end_quotes_date"]?>"  onclick="this.select();">

                      </div>
                                                                    
          </div>





  </div>
    
  </div>


</div>
</div>
</div>

<script src="js/jquery-3.3.1.js"></script>  
<script src="js/bootstrap.min.js"></script>

</body>
</html>
<?php }