<style>
body{
  font-size: 0.8rem;
}
.text-center{
        text-align: center;
            }
   table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > td:first-child:before, table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > th:first-child:before{
        margin-top: 2px; 
   }
   .page-title{
        letter-spacing: -1px;
        font-size: 0.9rem;
        margin: 14px 0 25px;
   }
  .tab-pane{
    border: 1px solid #dee2e6;
    padding: 5px;
  }
  .dtr-data{
    padding-left: 14px;
  }
  .control a:not(:first-child) {
    margin-left: 10px;
  }
  .navbar-dark .navbar-nav .nav-link {
    color: rgba(255,255,255,.7);
}
.btn-group{
  position: absolute;
}

.btn-group .btn{
  font-size: 0.7rem;
}

.font-large-2 {
    font-size: 3rem!important;
}

.white {
    color: #FFF!important;
}
.text-bold-400 {
  font-weight: 700;
} 
.stat-value{
  font-size: 1rem;  
  padding: 0 10px 5px 10px;
}
.stat-label{
  font-size: 0.7rem;
  padding: 5px 10px 0 10px;
  font-weight: 700;
}
.stat-icon{
  margin-top: 5px;
}
.progress-bar {
  text-align: left;
  transition-duration: 2s;
} 
.stat-card{
  cursor:pointer;
}

.page-item.active .page-link {
    z-index: 1;
    color: #fff;
    background-color: #dc3545!important;
    border-color: #dc3545!important;
}
.page-link{
  color:#dc3545;
}
.page-item.active .page-link {
  
  color:#fff !important;
}

</style>
<header><?php /* fixed-top*/ ?>
      <nav class="navbar navbar-expand-md navbar-dark  pl-0" style="background-color: #E1251B">
        <a class="navbar-brand w-auto" href="#">
          <img src="images/terpel+logo.svg" class="w-75"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
         <ul class="nav navbar-nav">
              <li class="nav-item <? echo ($selected_menu == 'dashboard')?'active':'';?>">
                  <a class="nav-link" href="dashboard.php">Dashboard </a>
              </li>
              <li class="nav-item  <? echo ($selected_menu == 'registro')?'active':'';?>">
                  <a class="nav-link" href="list.php">Registros </a>
              </li>
          </ul>
          <ul class="navbar-nav ml-auto p-2">
            <li class="nav-item">
              <a class="nav-link disabled" style="color:#fff" href="#"><i class="fal fa-user mr-1"></i> <?=mb_strtolower($_SESSION['email'], 'UTF-8');?></a>
            </li>
             
          </ul>
          <a class="btn btn-outline-warning" href="php/logout.php">Salir</a>

          <!-- <form class="form-inline mt-2 mt-md-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          </form> -->

        </div>
      </nav>
    </header>